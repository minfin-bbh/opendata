#!/usr/bin/env python3
# =============================================================================
__author__ = 'Christian C. Schouten'
__copyright__ = 'Copyright 2022, Ministerie van Financiën'
__license__ = 'CC-BY'
__version__ = '0.2.0'
__maintainer__ = 'Rik Peters'
__email__ = 'r.j.peters@minfin.nl'
__status__ = 'Prototype'
# =============================================================================
"""Source file for the data cleaning functions"""
# =============================================================================
import opendata.config as config
from opendata.datainput import readIbosData, executeApiCommand
import logging
import pandas as pd
import numpy as np
import re
import os
# =============================================================================

# Maak de inputdata schoon in stappen
def cleanInputData(inputdata, fase, inputstyle):
    logging.debug("Begin functie: cleanInputData")
    df = inputdata.copy()

    # Roep alle functies successievelijk aan
    df = removeEmptyLines(df)
    #df = normalizeIbos(df)
    df = setNumericColumns(df)

    if inputstyle == 'swr':
        df = cleanDataBasic1(df, fase)    
        
    df = cleanDataBasic2(df, fase)
    df = cleanDataBasic3(df, fase)
    #df = normalizeIbos(df)
    df = fillSubTotals(df, fase)
    df = checkIbosTotals(df, fase)
    df = checkGrowthThreshold(df, fase)

    logging.debug("Einde functie: cleanInputData")
    return df




# Verwijder lege/ongewenste regels
def removeEmptyLines(inputdata):
    logging.debug("Begin functie: removeEmptyLines")
    df = inputdata.copy()

    try:
        start = df.shape[0]

        # Lege regels
        df = df.drop(df[(df['Artikelnaam'].isna()) & (df['IBOSnummer'].isna()) & (df['Omschrijving'].isna())].index)

        # Herhaalde headerregels
        df = df.drop(df[df['Artikelnaam'] == 'Artikelnaam'].index)

        # Alleen artikelnaam
        df = df.drop(df[(df['Artikelnaam'].notna()) & (df['IBOSnummer'].isna()) & (df['Omschrijving'].isna())].index)

        end = df.shape[0]
        if start != end:
            logging.info(f"Er zijn {start - end} lege/ongewenste regels succesvol verwijderd")

        logging.info(f"Ingelezen dataset bevat {end} regels")
        if logging.root.level == logging.DEBUG:
            df.to_csv(os.path.join(config.settings['outputdir'], 'debug', 'removeEmptyLines.csv'))
    except Exception as e:
        logging.critical(f"Fout tijdens verwijderen van lege regels in dataset: {str(e)}")
    logging.debug("Einde functie: removeEmptyLines")
    return df

def normalizeIbos(inputdata):
    logging.debug("Begin functie: normalizeIbos")
    df = inputdata.copy()

    try:
        ibos = df['IBOSnummer'].tolist()
        output = []

        for item in ibos: 
            dummy = item.split('.')
            newdummy = ''
            for subitem in dummy:
                if len(subitem) == 4:
                    newdummy = newdummy + subitem   #voor jaartallen (eerste deel IBOS-nummer)
                else: 
                    if subitem=='0':
                        newdummy = newdummy + '.' + '0'
                    elif subitem[0]=='0':
                        newdummy = newdummy + '.' + str(subitem[1])
                    else: 
                        newdummy = newdummy + '.' + str(subitem)
            output.append(newdummy)

        df['IBOSnummer'] = output
        df.to_csv(os.path.join(config.settings['outputdir'], 'debug', 'IBOSfix.csv'))

        return df

    except Exception as e:
        logging.critical(f"Fout tijdens normaliseren van IBOS-nummer: {str(e)}")
    logging.debug("Einde functie: normalizeIbos")
    return df

# Stel de nummerkolommen in op basis van het IBOS-nummer
def setNumericColumns(inputdata):
    logging.debug("Begin functie: setNumericColumns")
    df = inputdata.copy()

    try:
        # Splits IBOS-nummer in zes delen
        df['Begrotingsjaar']         = df.apply(lambda x: splitIbosNumber(x['IBOSnummer'], 'jaar') if pd.notna(x['IBOSnummer']) else 0, axis=1)
        df['Hoofdstuknummer']        = df.apply(lambda x: splitIbosNumber(x['IBOSnummer'], 'BGG')  if pd.notna(x['IBOSnummer']) else 0, axis=1)
        df['Artikelnummer']          = df.apply(lambda x: splitIbosNumber(x['IBOSnummer'], 'A')    if pd.notna(x['IBOSnummer']) else 0, axis=1)
        df['Artikelonderdeelnummer'] = df.apply(lambda x: splitIbosNumber(x['IBOSnummer'], 'AO')   if pd.notna(x['IBOSnummer']) else 0, axis=1)
        df['Instrumentnummer']       = df.apply(lambda x: splitIbosNumber(x['IBOSnummer'], 'I')    if pd.notna(x['IBOSnummer']) else 0, axis=1)
        df['Detailnummer']           = df.apply(lambda x: splitIbosNumber(x['IBOSnummer'], 'D')    if pd.notna(x['IBOSnummer']) else 0, axis=1)

        logging.info("IBOS-nummer succesvol gesplitst")
        if logging.root.level == logging.DEBUG:
            df.to_csv(os.path.join(config.settings['outputdir'], 'debug', 'setNumericColumns.csv'))
    except Exception as e:
        logging.critical(f"Fout tijdens splitsen IBOS-nummer: {str(e)}")
    logging.debug("Einde functie: setNumericColumns")
    return df

# Splits een IBOS-nummer
def splitIbosNumber(ibosnummer, niveau):
    nummer = 0

    try:
        # Splits tussen de punten
        split_nummer = re.split(r"[.]", ibosnummer)
        if niveau == 'jaar'  and len(split_nummer) > 0: nummer = int(split_nummer[0])
        elif niveau == 'BGG' and len(split_nummer) > 1: nummer = re.sub(r"^0", "", split_nummer[1].upper()) # strip leading zero
        elif niveau == 'A'   and len(split_nummer) > 2: nummer = int(split_nummer[2])
        elif niveau == 'AO'  and len(split_nummer) > 3: nummer = int(split_nummer[3])
        elif niveau == 'I'   and len(split_nummer) > 4: nummer = int(split_nummer[4])
        elif niveau == 'D'   and len(split_nummer) > 5: nummer = int(split_nummer[5])

    except (ValueError, TypeError, Exception) as e:
        logging.error(f"Inputfout tijdens splitsen IBOS-nummer '{ibosnummer}': {str(e)}")
        return 0
    return nummer

# Haal de namen op van het artikelonderdeel, het instrument en het detail
def getRowLabels(inputdata, index, parent, ao, i, d):
    df = inputdata.copy()
    _ao = _i = _d = ''

    try:
        if d > 0:
            # Voor een detailregel alle drie ophalen
            grandparent = df.loc[df['Index'] == parent, 'Parent'].item()
            if len(grandparent) > 7 and int(df.loc[df['Index'] == grandparent, 'Artikelonderdeelnummer'].item()) > 0: # Grootouder is artikelonderdeel
                _ao = df.loc[df['Index'] == grandparent, 'Omschrijving'].item()
                _i = df.loc[df['Index'] == parent, 'Omschrijving'].item()
                _d = df.loc[df['Index'] == index, 'Omschrijving'].item()
            else: # Grootouder is artikel
                _ao = df.loc[df['Index'] == parent, 'Artikelnaam'].item()
                _i = df.loc[df['Index'] == parent, 'Omschrijving'].item()
                _d = df.loc[df['Index'] == index, 'Omschrijving'].item()
        elif i > 0:
            # Voor een instrumentregel zichzelf en ouder
            if len(parent) > 7 and int(df.loc[df['Index'] == parent, 'Artikelonderdeelnummer'].item()) > 0: # Ouder is artikelonderdeel
                _ao = df.loc[df['Index'] == parent, 'Omschrijving'].item()
                _i = df.loc[df['Index'] == index, 'Omschrijving'].item()
            else: # Ouder is artikel
                _ao = df.loc[df['Index'] == index, 'Artikelnaam'].item()
                _i = df.loc[df['Index'] == index, 'Omschrijving'].item()
        else:
            # Artikelonderdeel alleen zichzelf
            _ao = df.loc[df['Index'] == index, 'Omschrijving'].item()

    except Exception as e:
        logging.critical(f"Fout tijdens invullen labels van rij {index}: {str(e)}")
    return _ao, _i, _d

# Plaats de namen van de verschillende niveaus door de boom heen
def setLabelColumns(inputdata):
    logging.debug("Begin functie: setLabelColumns")
    df = inputdata.copy()

    try:
        # Haal de namen voor ao, i en d op via de ouder-kindrelatie
        df[['Artikelonderdeelnaam', 'Instrumentnaam', 'Detailnaam']] = df.sort_values(by='Index', ascending=False).apply(lambda x: getRowLabels(df, x['Index'], x['Parent'], x['Artikelonderdeelnummer'], x['Instrumentnummer'], x['Detailnummer']), axis=1, result_type='expand')

        logging.info("Namen van de verschillende niveaus succesvol toegevoegd")
        if logging.root.level == logging.DEBUG:
            df.to_csv(os.path.join(config.settings['outputdir'], 'debug', 'setLabelColumns.csv'))
    except Exception as e:
        logging.critical(f"Fout tijdens plaatsen van de namen uit de begroting: {str(e)}")
    logging.debug("Einde functie: setLabelColumns")
    return df

# Kleine schoonmaakacties, volgens 'oude' swr-format
def cleanDataBasic1(inputdata, fase):
    logging.debug("Begin functie: cleanDataBasic1")
    df = inputdata.copy()

    try:
        # Artikelnaam is verplicht
        dft = df[df['Artikelnaam'].isna()]
        if dft.shape[0] > 0:
            dft.apply(lambda x: logging.error(f"Fout tijdens opschonen inputdata: geen artikelnaam gevonden voor IBOS-nummer {x['IBOSnummer']}/{x['VUO']}"), axis=1)

        # IBOS-nummer is verplicht
        dft = df[df['IBOSnummer'].isna()]
        if dft.shape[0] > 0:
            dft.apply(lambda x: logging.error(f"Fout tijdens opschonen inputdata: geen IBOS-nummer gevonden op rij bij artikel '{x['Artikelnaam']}', omschrijving '{x['Omschrijving']}'"), axis=1)

        # Waarschuwing op dubbele IBOS-nummers
        dft = df[df.duplicated(subset=['IBOSnummer', 'VUO'])]
        if dft.shape[0] > 0:
            dft.apply(lambda x: logging.warning(f"Waarschuwing tijdens opschonen inputdata: dubbel IBOS-nummer gevonden {x['IBOSnummer']}/{x['VUO']}"), axis=1)

        # VUO is verplicht
        dft = df[df['VUO'].isna()]
        if dft.shape[0] > 0:
            dft.apply(lambda x: logging.error(f"Fout tijdens opschonen inputdata: geen VUO ingevuld voor IBOS-nummer {x['IBOSnummer']}/{x['VUO']}"), axis=1)

        # VUO moet V, U of O zijn
        df['VUO'] = df['VUO'].str.upper().str[:1]
        dft = df[~df['VUO'].isin(['V', 'U', 'O'])]
        if dft.shape[0] > 0:
            dft.apply(lambda x: logging.error(f"Fout tijdens opschonen inputdata: VUO niet juist ingevuld voor IBOS-nummer {x['IBOSnummer']}/{x['VUO']}"), axis=1)

        # Totaal is verplicht
        dft = df[df['Totaal'].isna()]
        if dft.shape[0] > 0:
            dft.apply(lambda x: logging.error(f"Fout tijdens opschonen inputdata: geen Totaal ingevuld voor IBOS-nummer {x['IBOSnummer']}/{x['VUO']}"), axis=1)

        # Totaal moet J of N zijn
        df['Totaal'] = df['Totaal'].str.upper().str[:1]
        dft = df[~df['Totaal'].isin(['J', 'N'])]
        if dft.shape[0] > 0:
            dft.apply(lambda x: logging.error(f"Fout tijdens opschonen inputdata: indicatie totaalregel niet juist ingevuld voor IBOS-nummer {x['IBOSnummer']}/{x['VUO']}"), axis=1)

        # Totaalregel met ingevuld detailnummer
        dftemp = df.copy()
        dftemp['Detailnummer'].fillna(0, inplace = True)
        dft = dftemp.query("Totaal == 'J' & (Detailnummer != 0)")
        if dft.shape[0] > 0:
            dft.apply(lambda x: logging.error(f"Fout tijdens opschonen inputdata: totaalregel met ingevuld detailnummer voor IBOS-nummer {x['IBOSnummer']}/{x['VUO']}"), axis=1)
            dft.apply(lambda x: logging.error(f"Extra info: {x['Detailnummer']} is detailnummer voor {x['IBOSnummer']}/{x['VUO']}"), axis=1)


        # Totaalregel zonder onderliggende regels
        for index, row in df.query("Totaal == 'J'").iterrows():
            cnt = -1
            if row['Instrumentnummer'] != 0:
                cnt = df.query(f"Totaal == 'N' & Hoofdstuknummer == '{row['Hoofdstuknummer']}' & Artikelnummer == {row['Artikelnummer']} & VUO == '{row['VUO']}' & Artikelonderdeelnummer == {row['Artikelonderdeelnummer']} & Instrumentnummer == {row['Instrumentnummer']}").shape[0]
            elif row['Artikelonderdeelnummer'] != 0:
                cnt = df.query(f"Totaal == 'N' & Hoofdstuknummer == '{row['Hoofdstuknummer']}' & Artikelnummer == {row['Artikelnummer']} & VUO == '{row['VUO']}' & Artikelonderdeelnummer == {row['Artikelonderdeelnummer']}").shape[0]
            else:
                cnt = df.query(f"Totaal == 'N' & Hoofdstuknummer == '{row['Hoofdstuknummer']}' & Artikelnummer == {row['Artikelnummer']} & VUO == '{row['VUO']}'").shape[0]
            if cnt == 0:
                logging.error(f"Fout tijdens opschonen inputdata: totaalregel zonder onderliggende regels voor IBOS-nummer {row['IBOSnummer']}/{row['VUO']}")

        # Detailregel met uitsplitsing
        for index, row in df.query("Totaal == 'N'").iterrows():
            cnt = -1
            if row['Detailnummer'] != 0:
                pass # Zelfde check als op uniciteit anders
            elif row['Instrumentnummer'] != 0:
                cnt = df.query(f"Hoofdstuknummer == '{row['Hoofdstuknummer']}' & Artikelnummer == {row['Artikelnummer']} & VUO == '{row['VUO']}' & Artikelonderdeelnummer == {row['Artikelonderdeelnummer']} & Instrumentnummer == {row['Instrumentnummer']}").shape[0]
                if cnt > 1 and fase == config.Totaal: df.loc[index, 'Totaal'] = 'J'
            elif row['Artikelonderdeelnummer'] != 0:
                cnt = df.query(f"Hoofdstuknummer == '{row['Hoofdstuknummer']}' & Artikelnummer == {row['Artikelnummer']} & VUO == '{row['VUO']}' & Artikelonderdeelnummer == {row['Artikelonderdeelnummer']}").shape[0]
                if cnt > 1 and fase == config.Totaal: df.loc[index, 'Totaal'] = 'J'
            else:
                cnt = df.query(f"Hoofdstuknummer == '{row['Hoofdstuknummer']}' & Artikelnummer == {row['Artikelnummer']} & VUO == '{row['VUO']}'").shape[0]
                if cnt > 1 and fase == config.Totaal: df.loc[index, 'Totaal'] = 'J'
            if cnt > 1 and fase < config.Totaal:
                logging.error(f"Fout tijdens opschonen inputdata: detailregel met onderliggende regels voor IBOS-nummer {row['IBOSnummer']}/{row['VUO']}")

        # Omschrijving is verplicht 
        dft = df[df['Omschrijving'].isna()]
        if dft.shape[0] > 0:
            dft.apply(lambda x: logging.error(f"Fout tijdens opschonen inputdata: geen omschrijving gevonden voor IBOS-nummer {x['IBOSnummer']}/{x['VUO']}"), axis=1)


    except Exception as e:
        logging.critical(f"Fout tijdens schoonmaken (basis1) van input data: {str(e)}")
    logging.debug("Einde functie: cleanDataBasic1")
    return df

#Kleine schoonmaakoperaties die voor ieder type input (swr of datahub) zinnig zijn.
def cleanDataBasic2(inputdata, fase):
    logging.debug("Begin functie: cleanDataBasic2")
    df = inputdata.copy()

    try:
        # Vul lege tekstkolommen met een lege string om door te gaan
        strcols = ['Artikelnaam', 'IBOSnummer', 'VUO', 'Totaal', 'Omschrijving']
        df[strcols] = df[strcols].fillna('')

        columns = []
        if fase == config.OWB:  columns = config.owbcols
        elif fase == config.O1: columns = config.o1cols
        elif fase == config.PS: columns = config.pscols
        elif fase == config.O2: columns = config.o2cols
        elif fase == config.JV: columns = config.jvcols

        # Rond alle getallen af naar hele duizendtallen
        for col in columns: df[col] = pd.to_numeric(df[col], errors='coerce').round().astype(float)

        # Vul lege getalkolommen met 0, maar alleen voor detailregels ivm berekening subtotalen
        df.loc[df['Totaal'] == 'N', columns] = df.loc[df['Totaal'] == 'N', columns].fillna(0).astype('int64')

        # Controleer dat het juiste jaar is ingevuld
        dft = df[df['Begrotingsjaar'].astype('int64') != int(config.settings['jaar'])]
        if dft.shape[0] > 0:
            dft.apply(lambda x: logging.warning(f"Waarschuwing op inputdata: verkeerd jaar {x['Begrotingsjaar']} gevonden voor IBOS-nummer {x['IBOSnummer']}/{x['VUO']}"), axis=1)
        df['Begrotingsjaar'] = config.settings['jaar'] # Overschrijf met correct jaar

        logging.info("Input data succesvol schoongemaakt (basishygiene)")
        if logging.root.level == logging.DEBUG:
            df.to_csv(os.path.join(config.settings['outputdir'], 'debug', f'cleanDataBasic_{fase}.csv'))
    except Exception as e:
        logging.critical(f"Fout tijdens schoonmaken (basis2) van input data: {str(e)}")
    logging.debug("Einde functie: cleanDataBasic2")
    return df

#Extra schoonmaakactie die wat meer tijd kost
def cleanDataBasic3(inputdata, fase):
    logging.debug("Begin functie: cleanDataBasic3")

    df = inputdata.copy()
    if fase == config.OWB:  numcols = config.owbcols
    elif fase == config.O1: numcols = config.o1cols
    elif fase == config.PS: numcols = config.pscols
    elif fase == config.O2: numcols = config.o2cols
    elif fase == config.JV: numcols = config.jvcols

    elif fase == "OWB":     numcols = config.owbcols
    elif fase == "O1": numcols = config.o1cols
    elif fase == "PS": numcols = config.pscols
    elif fase == "O2": numcols = config.o2cols
    elif fase == "JV": numcols = config.jvcols

    for col in numcols: 

        df[col]=df[col].str.decode("utf-8").replace(u"\u2012", "-")
        df[col]=df[col].replace('â€’','-', regex=True) #Bizarro-karakters voor min worden een 'gewone' min (UTF-8: U+002D)
        df[col]=df[col].replace('‒','-', regex=True) #Bizarro-karakters voor min worden een 'gewone' min (UTF-8: U+002D)
        df[col]=df[col].replace('[^0-9-]+','', regex=True).astype('int64', errors='ignore')
    logging.debug("Begin functie: cleanDataBasic3")

    return df


def cleanInstruments(inputdata):
    logging.debug("Begin functie: cleanInstruments")
    df = inputdata.copy()

    try:
        # Pre-approved values (and partial matches)
        afk_instrumenten = ['subsidie', 'lening', 'garantie', 'bekostiging', 'inkomensoverdracht', 'opdracht', 'agentschap', 'zbo', 'overheden', 'nationale organisaties', 'hoofdstuk',  'sociale fonds', 'vergoedingen', 'begrotingsreserve', 'persone', 'materi', 'rente', 'vermogens', 'mutaties', 'institutionele', 'nog te verdelen', 'geheim' , 'fonds', 'ontvangst']
        instrumenten =  ['Subsidies (regelingen)', 'Leningen', 'Garanties', 'Bekostiging', 'Inkomensoverdracht', 'Opdrachten', 'Bijdrage aan agentschappen', "Bijdrage aan ZBO's/RWT's", 'Bijdrage aan medeoverheden', 'Bijdrage aan (inter)nationale organisaties', 'Bijdrage aan (andere) begrotingshoofdstukken',  'Bijdrage aan sociale fondsen', '(Schade)vergoedingen', 'Storting/onttrekking begrotingsreserve', 'Personele uitgaven', 'Materiële uitgaven', 'Rente', 'Vermogenverschaffing/-onttrekking', "Mutaties in rekening-courant en deposito's", 'Institutionele inrichting', 'Nog te verdelen', 'Geheim' , 'Fonds', 'Ontvangsten']

        # Walk through instruments and try to match
        df['naam_instrument_oud'] = df['naam_instrument']
        df['bekend_instrument'] = False
        for i, (afk, vol) in enumerate(zip(afk_instrumenten, instrumenten)):
            df['lower'] = df['naam_instrument'].str.lower()
            df.loc[df['lower'].str.contains(afk), 'naam_instrument'] = vol # Overwrite a partial match with full string
        df['bekend_instrument'] = df['naam_instrument'].apply(lambda x: any([k in x for k in instrumenten]))
        df.loc[(df['naam_instrument'] == ''), 'bekend_instrument'] = True
        df = df.drop(columns='lower')

        # Gewijzigd instrument
        dft = df[df['naam_instrument'] != df['naam_instrument_oud']]
        if dft.shape[0] > 0:
            dft.apply(lambda x: logging.warning(f"Gewijzigd instrument: '{x['naam_instrument_oud']}' --> '{x['naam_instrument']}' (IBOS-nummer: {x['IBOSnummer']})"), axis=1)
        df = df.drop(columns='naam_instrument_oud')

        # Onbekend instrument
        dft = df[df['bekend_instrument'] == False]
        if dft.shape[0] > 0:
            dft.apply(lambda x: logging.warning(f"Onbekend instrument: '{x['naam_instrument']}' (IBOS-nummer: {x['IBOSnummer']})"), axis=1)
        df = df.drop(columns='bekend_instrument')

        logging.info("Financiële instrumenten succesvol schoongemaakt")
        if logging.root.level == logging.DEBUG:
            df.to_csv(os.path.join(config.settings['outputdir'], 'debug', 'cleanInstruments.csv'))
    except Exception as e:
        logging.critical(f"Fout tijdens schoonmaken van financiële instrumenten: {str(e)}")
    logging.debug("Einde functie: cleanInstruments")
    return df

# Vul de lege subtotalen met de som van de onderliggende niet-totaalregels
def fillSubTotals(inputdata, fase, edits = True):
    # Bij optie edits = False pas je niets aan, maar geef je alleen waarschuwingen. 
     
    logging.debug("Begin functie: fillSubTotals")
    df = inputdata.copy()

    try:
        # Bepaal waardekolommen
        if fase == config.OWB: columns = ['OWBMin2', 'OWBMin1', 'StandOWB', 'OWBPlus1', 'OWBPlus2', 'OWBPlus3', 'OWBPlus4']
        elif fase == config.O1: columns = ['StandOWB', 'MutatieOWB', 'StandVB', 'MutatieO1', 'StandO1', 'MutatiePlus1', 'MutatiePlus2', 'MutatiePlus3', 'MutatiePlus4']
        elif fase == config.PS: columns = ['StandO1enISB', 'MutatiePSupp', 'StandPSupp']
        elif fase == config.O2: columns = ['StandVB', 'StandVO1', 'MutatieO2MJN', 'MutatieO2Overig', 'StandO2']
        elif fase == config.JV: columns = ['RealisatieMin4', 'RealisatieMin3', 'RealisatieMin2', 'RealisatieMin1', 'Realisatie', 'StandVB', 'Verschil']

        # Indexniveaus
        index3 = ['Hoofdstuknummer', 'Artikelnummer', 'VUO']
        index4 = ['Hoofdstuknummer', 'Artikelnummer', 'VUO', 'Artikelonderdeelnummer']
        index5 = ['Hoofdstuknummer', 'Artikelnummer', 'VUO', 'Artikelonderdeelnummer', 'Instrumentnummer']
        index6 = ['Hoofdstuknummer', 'Artikelnummer', 'VUO', 'Artikelonderdeelnummer', 'Instrumentnummer', 'Detailnummer']

        # Bereken subtotalen op instrumentniveau
        dft = df[df['Totaal'] == 'N'].groupby(index5)[columns].sum() # Sommeer de detailregels
        dft['Detailnummer'] = 0 # Om te kunnen mergen
        df = df.merge(dft, 'left', on=index6, suffixes=('', '_i')) # Voeg de subtotalen in

        # Controleer dat de niet lege subtotalen op instrumentniveau overeenkomen
        qry = f"(Totaal == 'J' & Artikelnummer != 0 & Artikelonderdeelnummer != 0 & Instrumentnummer != 0) & ("
        for col in columns: qry += f"((abs({col} - {col}_i) > 3) & {col}.notnull()) | " # Sta afrondingsverschillen toe tot 3
        qry = qry[:-3] + ')'
        dft = df.query(qry, engine='python')
        if dft.shape[0] > 0:
            dft.apply(lambda x: logging.error(f"Optelling van een van de {config._faseToName.get(fase)}-kolommen klopt niet voor IBOS-nummer {x['IBOSnummer']}/{x['VUO']})"), axis=1)

        # Overschrijf (alle) subtotalen op instrumentniveau
        if edits == True:
            qry = f"Totaal == 'J' & Instrumentnummer != 0"
            for col in columns: df.loc[df.eval(qry), col] = df[col + '_i']
            df.drop(df.filter(regex='_i$').columns, axis=1, inplace=True)

        # Bereken subtotalen op artikelonderdeelniveau
        dft = df[df['Totaal'] == 'N'].groupby(index4)[columns].sum()
        dft['Instrumentnummer'] = 0
        df = df.merge(dft, 'left', on=index5, suffixes=('', '_ao'))

        # Controleer dat de niet lege subtotalen op artikelonderdeelniveau overeenkomen
        qry = f"(Totaal == 'J' & Artikelonderdeelnummer != 0 & Instrumentnummer == 0) & ("
        for col in columns: qry += f"((abs({col} - {col}_ao) > 3) & {col}.notnull()) | "
        qry = qry[:-3] + ')'
        dft = df.query(qry, engine='python')
        if dft.shape[0] > 0:
            dft.apply(lambda x: logging.error(f"Optelling van een van de {config._faseToName.get(fase)}-kolommen klopt niet voor IBOS-nummer {x['IBOSnummer']}/{x['VUO']})"), axis=1)

        # Overschrijf (alle) subtotalen op artikelonderdeelniveau
        if edits == True:
            qry = f"Totaal == 'J' & Artikelonderdeelnummer != 0 & Instrumentnummer == 0"
            for col in columns: df.loc[df.eval(qry), col] = df[col + '_ao']
            df.drop(df.filter(regex='_ao$').columns, axis=1, inplace=True)

        # Bereken subtotalen op artikelniveau
        dft = df[df['Totaal'] == 'N'].groupby(index3)[columns].sum()
        dft['Artikelonderdeelnummer'] = 0
        df = df.merge(dft, 'left', on=index4, suffixes=('', '_a'))

        # Controleer dat de niet lege subtotalen op artikelniveau overeenkomen
        qry = f"(Artikelonderdeelnummer == 0 & Instrumentnummer == 0) & ("
        for col in columns: qry += f"(abs({col} - {col}_a) > 3) | "
        qry = qry[:-3] + ')'
        dft = df.query(qry)
        if dft.shape[0] > 0:
            dft.apply(lambda x: logging.error(f"Optelling van een van de {config._faseToName.get(fase)}-kolommen klopt niet voor IBOS-nummer {x['IBOSnummer']}/{x['VUO']})"), axis=1)

        # Overschrijf (alle) subtotalen op artikelniveau
        if edits == True:
            qry = f"Artikelonderdeelnummer == 0 & Instrumentnummer == 0"
            for col in columns: df.loc[df.eval(qry), col] = df[col + '_a']
            df.drop(df.filter(regex='_a$').columns, axis=1, inplace=True)

        logging.info("Lege subtotalen succesvol ingevuld")
        if logging.root.level == logging.DEBUG:
            df.to_csv(os.path.join(config.settings['outputdir'], 'debug', f'fillSubTotals_{fase}.csv'))
            
    except Exception as e:
        if edits == True:
            logging.critical(f"Fout tijdens vullen van lege subtotalen: {str(e)}")
        else: 
            logging.critical(f"Fout tijdens het controleren van subtotalen: {str(e)}")
    logging.debug("Einde functie: fillSubTotals")
    return df

# Check totalen op hoofdstuk- en artikelniveau tegen IBOS
def checkIbosTotals(inputdata, fase):
    logging.debug("Begin functie: checkIbosTotals")
    df = inputdata.copy()

    # Standen matchen alleen met de huidige fase
    if fase != config.settings['fase']: return inputdata

    try:
        # Lees IBOS-data in en zet datatypes vast
        ibosdata = readIbosData()
        ibosdata['Hoofdstuknummer'] = ibosdata['Hoofdstuknummer'].str.upper() # Zeker weten dat beide in hoofdletters is
        ibosdata['VUO'] = ibosdata['VUO'].str.upper()
        cols = ['Artikelnummer', 'Artikelonderdeelnummer', 'Instrumentnummer', 'Detailnummer']
        ibosdata[cols] = ibosdata[cols].fillna(0).astype('int64')

        # IBOS-data invoegen
        df = df.merge(ibosdata, 'left', on=['Hoofdstuknummer', 'Artikelnummer', 'VUO', 'Artikelonderdeelnummer', 'Instrumentnummer', 'Detailnummer'], suffixes=('', '_ibos'))

        # Bepaal kolom om op te tellen
        value = 'StandOWB'
        if fase == config.O1: value = 'StandO1'
        elif fase == config.O2: value = 'StandO2'
        elif fase == config.PS: value = 'StandPSupp'
        elif fase == config.JV: value = 'Realisatie'

        # Totalen op hoofdstukniveau
        dfn = df[df['Totaal'] == 'N']
        df_btabel = dfn.groupby(['Hoofdstuknummer', 'VUO'])[[value]].sum().reset_index()
        df_btabel = df_btabel[df_btabel[value] > 0]
        df_ibos = ibosdata.groupby(['Hoofdstuknummer', 'VUO'])[['ibos_bedrag']].sum().reset_index()
        df_ibos = df_ibos[df_ibos['ibos_bedrag'] > 0]
        dft = df_btabel.merge(df_ibos, 'left', on=['Hoofdstuknummer', 'VUO'])
        dft = dft[dft[value].round() != dft['ibos_bedrag'].round()]
        if dft.shape[0] > 0:
            dft.apply(lambda x: logging.error(f"Totaalbedrag van hoofdstuk {x['Hoofdstuknummer']}/{x['VUO']} ({x[value]}) verschilt met IBOS ({x['ibos_bedrag']})"), axis=1)
        if logging.root.level == logging.DEBUG:
            dft.to_csv(os.path.join(config.settings['outputdir'], 'debug', f'checkIbosTotals_H.csv'))

        # Totalen op artikelniveau
        df_btabel = dfn.groupby(['Hoofdstuknummer', 'Artikelnummer', 'VUO'])[[value]].sum().reset_index()
        df_btabel = df_btabel[df_btabel[value] > 0]
        df_ibos = ibosdata.groupby(['Hoofdstuknummer', 'Artikelnummer', 'VUO'])[['ibos_bedrag']].sum().reset_index()
        df_ibos = df_ibos[df_ibos['ibos_bedrag'] > 0]
        dft = df_btabel.merge(df_ibos, 'left', on=['Hoofdstuknummer', 'Artikelnummer', 'VUO'])
        dft = dft[dft[value].round() != dft['ibos_bedrag'].round()]
        if dft.shape[0] > 0:
            dft.apply(lambda x: logging.error(f"Totaalbedrag van hoofdstuk {x['Hoofdstuknummer']}, artikel {x['Artikelnummer']}/{x['VUO']} ({x[value]}) verschilt met IBOS ({x['ibos_bedrag']})"), axis=1)
        if logging.root.level == logging.DEBUG:
            dft.to_csv(os.path.join(config.settings['outputdir'], 'debug', f'checkIbosTotals_A.csv'))

        logging.info("Alle data checks succesvol doorlopen")
    except Exception as e:
        logging.critical(f"Fout tijdens IBOS checks: {str(e)}")
    logging.debug("Einde functie: checkIbosTotals")
    return inputdata # Geen databewerking geweest

# Check of het totaal (per instrument) niet meer is gestegen dan een drempelwaarde
# Om wisselende IBOS-nummers te ondervangen wordt gesommeerd per instrument
def checkGrowthThreshold(inputdata, fase):
    logging.debug("Begin functie: checkGrowthThreshold")
    df = inputdata.copy()

    try:
        # Kolomnaam op basis van begrotingsfase
        value = 'StandOWB'
        if fase == config.O1: value = 'StandO1'
        elif fase == config.PS: value = 'StandPSupp'
        elif fase == config.O2: value = 'StandO2'
        elif fase == config.JV: value = 'Realisatie'

        # Sommeer jaar t per instrument
        df = df.groupby(['Hoofdstuknummer', 'Artikelnummer', 'VUO', 'Artikelonderdeelnummer', 'Instrumentnummer'])[[value]].sum().reset_index()

        # Haal jaar t-1 uit de API
        r = executeApiCommand(command='R', data={'begrotingsjaar': config.settings['jaar'] - 1})
        df_min1 = pd.DataFrame(r['value'])

        if df_min1.shape[0] > 0: # Als niet leeg...
            # Typesafe input for t-1
            df_min1['Hoofdstuknummer'] = df_min1.apply(lambda x: config._hoofdstukToKey.get(x['Hoofdstuknummer']), axis=1)
            df_min1.loc[df_min1['Artikelnummer'] == '', 'Artikelnummer'] = 0
            df_min1.loc[df_min1['Artikelonderdeelnummer'] == '', 'Artikelonderdeelnummer'] = 0
            df_min1.loc[df_min1['Instrumentnummer'] == '', 'Instrumentnummer'] = 0
            df_min1['Artikelnummer'] = df_min1['Artikelnummer'].fillna(0).astype(float)
            df_min1['Artikelonderdeelnummer'] = df_min1['Artikelonderdeelnummer'].fillna(0).astype(float)
            df_min1['Instrumentnummer'] = df_min1['Instrumentnummer'].fillna(0).astype(float)
            df_min1[value] = df_min1[value].fillna(0).astype(float)

            # Sommeer per instrument
            df_min1 = df_min1.groupby(['Hoofdstuknummer', 'Artikelnummer', 'VUO', 'Artikelonderdeelnummer', 'Instrumentnummer'])[[value]].sum().reset_index()
            if logging.root.level == logging.DEBUG:
                df_min1.to_csv(os.path.join(config.settings['outputdir'], 'debug', f'checkGrowthTreshold_{fase}_t-1.csv'))

            # Merge t-1 in bij t en filter lege bedragen weg
            df = df.merge(df_min1, 'left', on=['Hoofdstuknummer', 'Artikelnummer', 'VUO', 'Artikelonderdeelnummer', 'Instrumentnummer'], suffixes=('_t', '_t-1'))
            df[value + '_t-1'] = df[value + '_t-1'].fillna(0).astype(float)
            df = df[df[value + '_t-1'].astype(int) != 0] # Voorkom delen door 0

            # Groei berekenen en controleren tegen drempelwaarde
            df['Groei'] = (((df[value + '_t'].astype(int) - df[value + '_t-1'].astype(int)) / df[value + '_t-1'].astype(int)) * 100).abs().round(1)
            dft = df[df['Groei'] > config.settings['drempel']]
            if dft.shape[0] > 0:
                dft.apply(lambda x: logging.warning(f"Groei ({x['Groei']}%) van instrument {x['Hoofdstuknummer']}.{x['Artikelnummer']}.{x['VUO']}.{x['Artikelonderdeelnummer']}.{x['Instrumentnummer']} is boven de drempelwaarde)"), axis=1)

        logging.info("Groei t.o.v. t-1 gecontroleerd a.d.h.v. drempelwaarde")
        if logging.root.level == logging.DEBUG:
            df.to_csv(os.path.join(config.settings['outputdir'], 'debug', f'checkGrowthTreshold_{fase}.csv'))
    except Exception as e:
        logging.critical(f"Fout tijdens controle met drempelwaarde: {str(e)}")
    logging.debug("Einde functie: checkGrowthThreshold")
    return inputdata # Geen databewerking geweest

# Check of de interne verwijzingen en rekenregels gerespecteerd zijn
# Om wisselende IBOS-nummers over de jaren te ondervangen wordt bij verwijzing naar t-1 gesommeerd per instrument
def checkInternalConsistency(inputdata, fase):
    logging.debug("Begin functie: checkInternalConsistency")
    df = inputdata.copy()

    try:
        if fase == config.OWB:
            # De kolom OWBMin2 wordt gevuld met de stand uit de slotwet over het jaar t-2

            # Sommeer jaar t per instrument
            df = df.groupby(['Hoofdstuknummer', 'Artikelnummer', 'VUO', 'Artikelonderdeelnummer', 'Instrumentnummer'])[['OWBMin2']].sum().reset_index()

            # Haal jaar t-2 uit de API
            r = executeApiCommand(command='R', data={'begrotingsjaar': config.settings['jaar'] - 2, 'begrotingsfase': 'JV'})
            df_min2 = pd.DataFrame(r['value'])

            if df_min2.shape[0] > 0: # Als niet leeg...
                # Typesafe input for t-2
                df_min2.loc[df_min2['Artikelnummer'] == '', 'Artikelnummer'] = 0
                df_min2.loc[df_min2['Artikelonderdeelnummer'] == '', 'Artikelonderdeelnummer'] = 0
                df_min2.loc[df_min2['Instrumentnummer'] == '', 'Instrumentnummer'] = 0
                df_min2['Artikelnummer'] = df_min2['Artikelnummer'].fillna(0).astype(float)
                df_min2['Artikelonderdeelnummer'] = df_min2['Artikelonderdeelnummer'].fillna(0).astype(float)
                df_min2['Instrumentnummer'] = df_min2['Instrumentnummer'].fillna(0).astype(float)
                df_min2['RealisatieTMin2'] = df_min2['RealisatieTMin2'].fillna(0).astype(float)

                # Sommeer per instrument
                df_min2 = df_min2.groupby(['Hoofdstuknummer', 'Artikelnummer', 'VUO', 'Artikelonderdeelnummer', 'Instrumentnummer'])[['RealisatieTMin2']].sum().reset_index()
                if logging.root.level == logging.DEBUG:
                    df_min2.to_csv(os.path.join(config.settings['outputdir'], 'debug', f'checkInternalConsistency_{fase}_t-2.csv'))

                # Merge t-2 in bij t en filter lege bedragen weg
                df = df.merge(df_min2, 'left', on=['Hoofdstuknummer', 'Artikelnummer', 'VUO', 'Artikelonderdeelnummer', 'Instrumentnummer'], suffixes=('_t', '_t-2'))
                df['RealisatieTMin2'] = df['RealisatieTMin2'].fillna(0).astype(float)
                df = df[df['RealisatieTMin2'] != 0]

                # Controleer waar er een verschil zit
                dft = df.query("abs(OWBMin2 - RealisatieTMin2) > 1")
                if dft.shape[0] > 0:
                    dft.apply(lambda x: logging.error(f"Stand OWBMin2 ({x['OWBMin2']}) voor IBOS-nummer {x['IBOSnummer']}/{x['VUO']} (op instrumentniveau) komt niet overeen met realisatie uit t-2 ({x['RealisatieTMin2']})"), axis=1)

            # De kolom OWBMin1 wordt gevuld met de meest recente stand (na de augustusbrief). Is dus stand vastgestelde O1 plus eventuele ISB's en daarmee niet via de dataset te controleren.

        elif fase == config.O1:
            # De kolom StandOWB (suffix '_o1') moet overeenkomen met de waarde die al in de dataset zit
            dft = df.query("StandOWB.notnull() & abs(StandOWB_o1 - StandOWB) > 1", engine='python')
            if dft.shape[0] > 0:
                dft.apply(lambda x: logging.error(f"StandOWB in O1 ({x['StandOWB_o1']}) komt niet overeen met StandOWB in OWB ({x['StandOWB']}) voor IBOS-nummer {x['IBOSnummer']}/{x['VUO']}"), axis=1)

            # De kolom StandVB moet overeenkomen met de som van StandOWB en MutatieOWB
            dft = df.query("abs(StandVB - StandOWB_o1 - MutatieOWB) > 1")
            if dft.shape[0] > 0:
                dft.apply(lambda x: logging.error(f"StandVB ({x['StandVB']}) is niet gelijk aan de som ({x['StandOWB_o1']+x['MutatieOWB']}) van StandOWB en MutatieOWB voor IBOS-nummer {x['IBOSnummer']}/{x['VUO']}"), axis=1)

            # De kolom StandO1 moet overeenkomen met de som van StandVB en MutatieO1
            dft = df.query("abs(StandO1 - StandVB - MutatieO1) > 1")
            if dft.shape[0] > 0:
                dft.apply(lambda x: logging.error(f"StandO1 ({x['StandO1']}) is niet gelijk aan de som ({x['StandVB']+x['MutatieO1']}) van StandVB en MutatieO1 voor IBOS-nummer {x['IBOSnummer']}/{x['VUO']}"), axis=1)

            # Een nieuw IBOS-nummer kan geen StandOWB hebben
            dft = df.query("Index == 'Nieuw in O1' & StandOWB_o1 != 0 & Totaal == 'N'")
            if dft.shape[0] > 0:
                dft.apply(lambda x: logging.error(f"IBOS-nummer {x['IBOSnummer']}/{x['VUO']} is nieuw in O1 maar heeft wel een waarde voor StandOWB ({x['StandOWB_o1']})"), axis=1)

        elif fase == config.PS:
            # De kolom 'StandPSupp' moet overeenkomen met de som van StandO1enISB en MutatiePSupp

            df.astype({'StandO1enISB': 'int64', 'MutatiePSupp': 'int64', 'StandPSupp': 'int64'}, errors = 'ignore')
            # df['StandO1enISB'] = df['StandO1enISB'].astype('int64')
            # df['MutatiePSupp'] = df['MutatiePSupp'].astype('int64')
            # df['StandPSupp'] =  df['StandPSupp'].astype('int64')

            dft = df.query("abs(StandO1enISB - MutatiePSupp - StandPSupp) > 1")
            if dft.shape[0] > 0:
                dft.apply(lambda x: logging.error(f"StandPSupp ({x['SPSupp']}) is niet gelijk aan de som {x['StandO1enISB']+x['MutatiePSupp']} van StandO1enISB en MutatiePSupp voor IBOS-nummer {x['IBOSnummer']}/{x['VUO']}"), axis=1)


        elif fase == config.O2:
            # De kolom StandVB (suffix '_o2') moet overeenkomen met de waarde die al in de dataset zit
            dft = df.query("StandVB.notnull() & abs(StandVB_o2 - StandVB) > 1", engine='python')
            if dft.shape[0] > 0:
                dft.apply(lambda x: logging.error(f"StandVB in O2 ({x['StandVB_o2']}) komt niet overeen met StandVB in O1 ({x['StandVB']}) voor IBOS-nummer {x['IBOSnummer']}/{x['VUO']}"), axis=1)

            # De kolom StandO2 moet overeenkomen met de som van StandVO1, MutatieO2MJN en MutatieO2Overig
            dft = df.query("abs(StandO2 - StandVO1 - MutatieO2MJN - MutatieO2Overig) > 1")
            if dft.shape[0] > 0:
                dft.apply(lambda x: logging.error(f"StandO2 ({x['StandO2']}) is niet gelijk aan de som ({x['StandVO1']+x['MutatieO2MJN']+x['MutatieO2Overig']}) van StandVO1, MutatieO2MJN en MutatieO2Overig voor IBOS-nummer {x['IBOSnummer']}/{x['VUO']}"), axis=1)

            # Een nieuw IBOS-nummer kan geen StandVB hebben
            dft = df.query("Index == 'Nieuw in O2' & StandVB_o2 != 0 & Totaal == 'N'")
            if dft.shape[0] > 0:
                dft.apply(lambda x: logging.error(f"IBOS-nummer {x['IBOSnummer']}/{x['VUO']} is nieuw in O2 maar heeft wel een waarde voor StandVB ({x['StandVB_o2']})"), axis=1)


        elif fase == config.JV:
            # De kolom StandVB (suffix '_jv') moet overeenkomen met de waarde die al in de dataset zit
            dft = df.query("StandVB.notnull() & abs(StandVB_jv - StandVB) > 1", engine='python')
            if dft.shape[0] > 0:
                dft.apply(lambda x: logging.error(f"StandVB in JV ({x['StandVB_jv']}) komt niet overeen met StandVB in O1 ({x['StandVB']}) voor IBOS-nummer {x['IBOSnummer']}/{x['VUO']}"), axis=1)

            # De kolom Verschil moet overeenkomen met het verschil tussen Realisatie en StandVB
            dft = df.query("abs(Realisatie - StandVB_jv) - abs(Verschil) > 1")
            if dft.shape[0] > 0:
                dft.apply(lambda x: logging.error(f"Verschil ({x['Verschil']}) is niet gelijk aan het verschil ({x['Realisatie']-x['StandVB_jv']}) tussen Realisatie en StandVB voor IBOS-nummer {x['IBOSnummer']}/{x['VUO']}"), axis=1)

            # Er mogen in deze fase geen nieuwe IBOS-nummers opduiken
            dft = df.query("Index == 'Nieuw in JV' & Totaal == 'N'")
            dft = dft.query("Realisatie != 0 | StandVB != 0") # Wegfilteren oude jaren
            if dft.shape[0] > 0:
                dft.apply(lambda x: logging.error(f"Nieuw IBOS-nummer gevonden bij JV: {x['IBOSnummer']}/{x['VUO']}"), axis=1)

            # De kolommen RealisatieMin2 en RealisatieMin1 worden gecontroleerd adhv de bestaande data uit de API (t-3 en t-4 zijn minder belangrijk om te controleren)

            # Sommeer jaar t per instrument
            df = df.groupby(['Hoofdstuknummer', 'Artikelnummer', 'VUO', 'Artikelonderdeelnummer', 'Instrumentnummer'])[['RealisatieMin1', 'RealisatieMin2']].sum().reset_index()

            # Haal jaar t-1 uit de API
            r = executeApiCommand(command='R', data={'begrotingsjaar': config.settings['jaar'] - 1, 'begrotingsfase': 'JV'})
            df_min1 = pd.DataFrame(r['value'])

            if df_min1.shape[0] > 0: # Als niet leeg...
                # Typesafe input for t-1
                df_min1.loc[df_min1['Artikelnummer'] == '', 'Artikelnummer'] = 0
                df_min1.loc[df_min1['Artikelonderdeelnummer'] == '', 'Artikelonderdeelnummer'] = 0
                df_min1.loc[df_min1['Instrumentnummer'] == '', 'Instrumentnummer'] = 0
                df_min1['Artikelnummer'] = df_min1['Artikelnummer'].fillna(0).astype(float)
                df_min1['Artikelonderdeelnummer'] = df_min1['Artikelonderdeelnummer'].fillna(0).astype(float)
                df_min1['Instrumentnummer'] = df_min1['Instrumentnummer'].fillna(0).astype(float)
                df_min1['RealisatieTMin1'] = df_min1['RealisatieTMin1'].fillna(0).astype(float)

                # Sommeer per instrument
                df_min1 = df_min1.groupby(['Hoofdstuknummer', 'Artikelnummer', 'VUO', 'Artikelonderdeelnummer', 'Instrumentnummer'])[['RealisatieTMin1']].sum().reset_index()
                if logging.root.level == logging.DEBUG:
                    df_min1.to_csv(os.path.join(config.settings['outputdir'], 'debug', f'checkInternalConsistency_{fase}_t-1.csv'))

                # Merge t-1 in bij t en filter lege bedragen weg
                dft = df.merge(df_min1, 'left', on=['Hoofdstuknummer', 'Artikelnummer', 'VUO', 'Artikelonderdeelnummer', 'Instrumentnummer'], suffixes=('_t', '_t-1'))
                dft['RealisatieTMin1'] = dft['RealisatieTMin1'].fillna(0).astype(float)
                dft = dft[dft['RealisatieTMin1'] != 0]

                # Controleer waar er een verschil zit
                dft = dft[dft['RealisatieMin1'] != dft['RealisatieTMin1']]
                if dft.shape[0] > 0:
                    dft.apply(lambda x: logging.warning(f"RealisatieMin1 ({x['RealisatieMin1']}) voor instrument {x['Hoofdstuknummer']}.{x['Artikelnummer']}.{x['VUO']}.{x['Artikelonderdeelnummer']}.{x['Instrumentnummer']} komt niet overeen met Realisatie uit t-1 ({x['RealisatieTMin1']})"), axis=1)

            # Haal jaar t-2 uit de API
            r = executeApiCommand(command='R', data={'begrotingsjaar': config.settings['jaar'] - 2})
            df_min2 = pd.DataFrame(r['value'])

            if df_min2.shape[0] > 0: # Als niet leeg...
                # Typesafe input for t-1
                df_min2.loc[df_min2['Artikelnummer'] == '', 'Artikelnummer'] = 0
                df_min2.loc[df_min2['Artikelonderdeelnummer'] == '', 'Artikelonderdeelnummer'] = 0
                df_min2.loc[df_min2['Instrumentnummer'] == '', 'Instrumentnummer'] = 0
                df_min2['Artikelnummer'] = df_min2['Artikelnummer'].fillna(0).astype(float)
                df_min2['Artikelonderdeelnummer'] = df_min2['Artikelonderdeelnummer'].fillna(0).astype(float)
                df_min2['Instrumentnummer'] = df_min2['Instrumentnummer'].fillna(0).astype(float)
                df_min2['RealisatieTMin2'] = df_min2['RealisatieTMin2'].fillna(0).astype(float)

                # Sommeer per instrument
                df_min2 = df_min2.groupby(['Hoofdstuknummer', 'Artikelnummer', 'VUO', 'Artikelonderdeelnummer', 'Instrumentnummer'])[['RealisatieTMin2']].sum().reset_index()
                if logging.root.level == logging.DEBUG:
                    df_min2.to_csv(os.path.join(config.settings['outputdir'], 'debug', f'checkInternalConsistency_{fase}_t-2.csv'))

                # Merge t-2 in bij t en filter lege bedragen weg
                dft = df.merge(df_min2, 'left', on=['Hoofdstuknummer', 'Artikelnummer', 'VUO', 'Artikelonderdeelnummer', 'Instrumentnummer'], suffixes=('_t', '_t-2'))
                dft['RealisatieTMin2'] = dft['RealisatieTMin2'].fillna(0).astype(float)
                dft = dft[dft['RealisatieTMin2'] != 0]

                # Controleer waar er een verschil zit
                dft = dft[dft['RealisatieMin2'] != dft['RealisatieTMin2']]
                if dft.shape[0] > 0:
                    dft.apply(lambda x: logging.warning(f"RealisatieMin2 ({x['RealisatieMin2']}) voor instrument {x['Hoofdstuknummer']}.{x['Artikelnummer']}.{x['VUO']}.{x['Artikelonderdeelnummer']}.{x['Instrumentnummer']} komt niet overeen met Realisatie uit t-2 ({x['RealisatieTMin2']})"), axis=1)

        logging.info("Optellingen binnen data en interne verwijzingen gecontroleerd")
        if logging.root.level == logging.DEBUG:
            df.to_csv(os.path.join(config.settings['outputdir'], 'debug', f'checkInternalConsistency_{fase}.csv'))
    except Exception as e:
        logging.critical(f"Fout tijdens controle interne consistentie: {str(e)}")
    logging.debug("Einde functie: checkInternalConsistency")
    return inputdata # Geen databewerking geweest




# Filter: neem je h40 en 41 wel of niet mee? (Hangt af van boolean in config.settings['premies'])
def premieFilter(inputdata):
    logging.debug("Begin functie: premieFilter")
    jaar = config.settings['jaar']
    try:
        df = inputdata.copy()
        df = df[ df['IBOSnummer'].str.contains( f'{jaar}.40|{jaar}.41' )==False ].copy()

    except Exception as e:
        logging.critical(f"Fout tijdens het uitfilteren van premies: {str(e)}")
    logging.debug("Einde functie: premieFilter")
    return df


# Afgeschermd van de schoonmaak: databewerkingen
def manipulateData(inputdata, inclusief = False):
    logging.debug("Begin functie: manipulateData")
    df = inputdata.copy()
    if inclusief == False:
        df = premieFilter(df)
    else:

        try:
            # Verwijder XV.12 om dubbeltelling te voorkomen
            df.drop(df.loc[(df['Hoofdstuknummer'] == '15') & (df['Artikelnummer'] == 12)].index, inplace=True)

            # Wijzig XVI-41.11 ivm dubbel artikelnummer
            df.loc[(df['Hoofdstuknummer'] == '41') & (df['Artikelnummer'] == 11), 'Artikelnummer'] = 4111

            # Voeg 40 samen met XV and 41 met XVI (premies)
            df['Hoofdstuknummer'].replace({'40': '15', '41': '16'}, inplace=True)
            
            


            # Verwijder kader N
            fase = config.settings['fase']
            try: 
                kader_n = pd.read_excel(os.path.join(config.settings['inputdir'], 'supplement', f"{config.settings['jaar']}_{config._faseToName.get(fase)}_kader_n.xlsx"))
            except Exception as e:
                logging.critical(f"Bestand voor kader N niet gevonden (verwacht in in map {config.settings['inputdir']}/'zorg'/{config.settings['jaar']}_{config._faseToName.get(fase)}_kader_n.xlsx')")
            kader_n = normalizeIbos(kader_n)
            droplist = kader_n['IBOSnummer'].to_list()
            for i in droplist: 
                
                df = df[(df['IBOSnummer']!=i)]
            logging.info("Input data succesvol bewerkt")


        except Exception as e:
            logging.critical(f"Fout tijdens manipuleren van data tbv versies inclusief/exclusief premies: {str(e)}")

    # Voeg IXA en IXB samen tot IX
    df['Hoofdstuknummer'].replace({'9A': '9', '9B': '9'}, inplace=True)
    
    if logging.root.level == logging.DEBUG:
        df.to_csv(os.path.join(config.settings['outputdir'], 'debug', f'manipulateData.csv'))
        
    logging.debug("Einde functie: manipulateData")
    return df

