#!/usr/bin/env python3
# =============================================================================
__author__ = 'Rik Peters'
__copyright__ = 'Copyright 2023, Ministerie van Financiën'
__license__ = 'CC-BY'
__version__ = '0.2.0'
__maintainer__ = 'Rik Peters'
__email__ = 'r.j.peters@minfin.nl'
__status__ = 'Prototype'
# =============================================================================
"""Source file for the extra sanity checks"""
# =============================================================================
import config
#import opendata.config as config
#from opendata.datacleaning import fillSubTotals
import logging
import pandas as pd
import numpy as np
import re
import os
# =============================================================================

def crossChecker(dataset):
    # Functie om artikelstanden in de outputdata te vergelijken met begrotingsstanden in de witte stukken (ws). 
    
    data = dataset.copy()
    jaar = config.settings['jaar']
    fase = config._faseToName[config.settings['fase']]

    # Begrotingsstaten uit witte stukken importeren
    wsdata = pd.read_csv(os.path.join(config.settings['inputdir'], 'supplement', f"{jaar}_{fase}_wittestukken.csv"))

    try:
        hoofdstukken = []
        for i in data['Hoofdstuknummer']:
            if not '9' in i:
                hoofdstukken.append(config._hoofdstukToLabel[i])
            else: 
                hoofdstukken.append('IX')
        data['Hoofdstuknummer'] = hoofdstukken
    except: 
        print("Fout in hoofdstuknummer converteren naar Romeins getal")


    # Dataset voor check reduceren tot alleen artikelniveau
    artikelen = data.query("Artikelonderdeelnummer == 0 & Instrumentnummer == 0 & Detailnummer == 0").copy()  

    # Schoonmaakactie op H3
    # Part 1: in de input uit de datahub zitten de data dubbel. IIIA, IIIB en IIIC bestaan, maar ook straightforward III
    dummylist = []
    for i in range(len(artikelen)):
        dummylist.append(artikelen['IBOSnummer'].iloc[i].split('.')[1])
    artikelen['dummy']=dummylist
    if len(artikelen[artikelen['dummy']=='3']) != 0:
        artikelen = artikelen[artikelen['dummy']!='3']


    #artikelen = artikelen[artikelen['IBOSnummer'].split('.')[1] not in ['3A','3B','3C']]
    

    h3 = artikelen[artikelen['Hoofdstuknummer']=='III'].copy()
    h3.reset_index(inplace=True, drop=True)
    overig = artikelen[artikelen['Hoofdstuknummer']!='III'].copy()
    for i in range(len(h3)):

        if h3.at[i, 'Artikelnaam'].lower().split()[0] == "eenheid":
            h3.at[i, 'Hoofdstuknummer'] = 'IIIA'
        elif h3.at[i, 'Artikelnaam'].lower().split()[0] == "kabinet":
            h3.at[i, 'Hoofdstuknummer'] = 'IIIB'
        elif h3.at[i, 'Artikelnaam'].lower().split()[0] == 'commissie':
            h3.at[i, 'Hoofdstuknummer'] = 'IIIC'

    artikelen = pd.concat([overig, h3], axis = 0)


    # Verder reduceren tot alleen de huidige stand
    #artikelen = artikelen[['Begrotingsjaar', 'VUO', 'Hoofdstuknummer', 'Artikelnummer', f'Stand{fase}']]




    # Vergelijking opstellen
    comparison = artikelen.merge(wsdata, 'outer', on=['Begrotingsjaar','Hoofdstuknummer', 'Artikelnummer', 'VUO'], suffixes=('', '_ws'))
    comparison.reset_index(inplace=True, drop=True)



    # Als IBOS geen stand heeft, en de witte stukken hebben '0', is het prima. In die gevallen behandelt de vergelijking de ontbrekende IBOSstand als =0.
    for i in range(len(comparison)):
        if comparison['StandOWB_ws'].loc[i] == 0 and np.isnan(comparison['StandOWB'].iloc[i]):
            comparison.at[i, 'StandOWB'] = 0

    # Zelfde check andersom, maar iets voorzichtiger: alleen als de stand op T-2 en/of T-1 geen 0 is mag je de stand T als 0 weergeven.
    # Als er dit jaar geen IBOSstand is, komt deze post mogelijk niet in de witte stukken voor, maar wel nog in IBOS vanwege de historische standen.
    for i in range(len(comparison)):
        if comparison['StandOWB'].loc[i] == 0 and np.isnan(comparison['StandOWB_ws'].iloc[i]):
            if comparison['OWBMin2'].iloc[i] !=0 or comparison['OWBMin1'].iloc[i]!=0: 
                comparison.at[i, 'StandOWB_ws'] = 0


    comparison = comparison[['Begrotingsjaar', 'VUO', 'Hoofdstuknummer', 'Artikelnummer', f'Stand{fase}', f'Stand{fase}_ws']]


    difference = (comparison.query(f"Stand{fase} != Stand{fase}_ws"))
    


    # Check 1: standen wel in ibos, niet in ws:
    dft = difference.query("StandOWB_ws.isna()", engine='python')
    if dft.shape[0] > 0:
        dft.apply(lambda x: logging.error(f"**** Wel in IBOS, niet in witte stukken: {x['Hoofdstuknummer']}.{x['Artikelnummer']}/{x['VUO']}. Stand in IBOS is {x['StandOWB']}"), axis=1)

    # Check 2: standen wel in ws, niet in ibos:
    dft = difference.query("StandOWB.isna()", engine='python')
    if dft.shape[0] > 0:
        dft.apply(lambda x: logging.error(f"**** Wel in witte stukken, niet in IBOS: {x['Hoofdstuknummer']}.{x['Artikelnummer']}/{x['VUO']}. Stand in witte stukken is {x['StandOWB_ws']}"), axis=1)

    # Check 3: verschil tussen ws en ibos:
    dft = difference.query("not StandOWB_ws.isna() & not StandOWB.isna()")
    if dft.shape[0] > 0:
        dft.apply(lambda x: logging.error(f"**** Verschil tussen witte stukken en IBOS: {x['Hoofdstuknummer']}.{x['Artikelnummer']}/{x['VUO']}. Stand in witte stukken is {x['StandOWB_ws']}. Stand in IBOS is {x['StandOWB']}"), axis=1)

    
    #difference.isna().sum())





    logging.info(f"**** ")
    # Check 1: evenveel artikelen in witte stukken als in ibosdata?
    if len(artikelen) != len(wsdata):
        logging.info(f"**** Niet evenveel artikelen in witte stukken als in ibosdata: {len(artikelen)} in artikelen, {len(wsdata)} in witte stukken.")
        if len(artikelen) > len(wsdata):
            pass

        elif len(artikelen) < len(wsdata):
            pass


    return 0

def sanityCheck(dataset):
    fase = config.settings['fase']
    logging.debug("Begin functie: sanity check")


    #fillSubTotals(dataset, fase = fase, edits = False)

    crossChecker(dataset)



    return 0

#data = pd.read_csv(os.path.join(config.settings['outputdir'], 'Budgettaire Tabel 2024.csv'))
data = pd.read_csv(r"c:\Users\Administrator\Documents\Pre-Prinsjes\Budgettaire Tabellen 2024-2024_incl.csv")
sanityCheck(data)