#!/usr/bin/env python3
# =============================================================================
__author__ = 'Christian C. Schouten'
__copyright__ = 'Copyright 2023, Ministerie van Financiën'
__license__ = 'CC-BY'
__version__ = '0.2.0'
__maintainer__ = 'Rik Peters'
__email__ = 'r.j.peters@minfin.nl'
__status__ = 'Prototype'
# =============================================================================
"""Script om index en parent/child-relaties in te stellen voor B-tabellen"""
# =============================================================================
import logging
import opendata.config as config
import os


# Voeg indexnummer toe
def setIndex(df):
    logging.debug("Begin functie: setIndex")
    jaar = config.settings['jaar']
    try:
        # Indexwaarde is: Jaar.Hoofdstuknummer.Volgnummer
        for i in range(df.shape[0]):
            df.loc[i, 'Index'] = f'{jaar}.' + f"{int(df.loc[i, 'Hoofdstuknummer'].strip('ABC')):02d}" + f'{i+1:04d}'
        if logging.root.level == logging.DEBUG:
            df.to_csv(os.path.join(config.settings['outputdir'], 'debug', 'setIndex.csv'))
    except Exception as e:
        logging.critical(f"Fout tijdens indexeren van databestand: {str(e)}")
    logging.debug("Einde functie: setIndex")
    return df['Index']



# Loop door de boom om alle ouder-kind relaties in te stellen
def setParent(df):
    logging.debug("Begin functie: setParent")

    try:
        # Begin met het hoofdstuknummer als topniveau
        df['Parent'] = df['Index'].str[:-4]

        for i in range(df.shape[0]):
            dft = None
            # IBOS-nummer met alle zes niveaus
            if df.loc[i, 'Detailnummer'] != 0:
                # Zoek zelfde IBOS-nummer op instrumentniveau
                dft = df[((df['VUO'] == df.loc[i, 'VUO']) & (df['Hoofdstuknummer'] == df.loc[i, 'Hoofdstuknummer']) & (df['Artikelnummer'] == df.loc[i, 'Artikelnummer']) & (df['Artikelonderdeelnummer'] == df.loc[i, 'Artikelonderdeelnummer']) & (df['Instrumentnummer'] == df.loc[i, 'Instrumentnummer']) & (df['Detailnummer'] == 0))]
                # Zoek zelfde IBOS-nummer op artikelonderdeelniveau indien instrumentniveau mist
                if dft.shape[0] < 1:
                    dft = df[((df['VUO'] == df.loc[i, 'VUO']) & (df['Hoofdstuknummer'] == df.loc[i, 'Hoofdstuknummer']) & (df['Artikelnummer'] == df.loc[i, 'Artikelnummer']) & (df['Artikelonderdeelnummer'] == df.loc[i, 'Artikelonderdeelnummer']) & (df['Instrumentnummer'] == 0))]
                # Zoek zelfde IBOS-nummer op artikelniveau indien artikelonderdeelniveau mist
                if dft.shape[0] < 1:
                    dft = df[((df['VUO'] == df.loc[i, 'VUO']) & (df['Hoofdstuknummer'] == df.loc[i, 'Hoofdstuknummer']) & (df['Artikelnummer'] == df.loc[i, 'Artikelnummer']) & (df['Artikelonderdeelnummer'] == 0))]
            # IBOS-nummer met vijf niveaus
            elif df.loc[i, 'Instrumentnummer'] != 0:
                # Zoek zelfde IBOS-nummer op artikelonderdeelniveau
                dft = df[((df['VUO'] == df.loc[i, 'VUO']) & (df['Hoofdstuknummer'] == df.loc[i, 'Hoofdstuknummer']) & (df['Artikelnummer'] == df.loc[i, 'Artikelnummer']) & (df['Artikelonderdeelnummer'] == df.loc[i, 'Artikelonderdeelnummer']) & (df['Instrumentnummer'] == 0))]
                # Zoek zelfde IBOS-nummer op artikelniveau indien artikelonderdeelniveau mist
                if dft.shape[0] < 1:
                    dft = df[((df['VUO'] == df.loc[i, 'VUO']) & (df['Hoofdstuknummer'] == df.loc[i, 'Hoofdstuknummer']) & (df['Artikelnummer'] == df.loc[i, 'Artikelnummer']) & (df['Artikelonderdeelnummer'] == 0))]
            # IBOS-nummer met vier niveaus
            elif df.loc[i, 'Artikelonderdeelnummer'] != 0:
                # Zoek zelfde IBOS-nummer op artikelniveau
                dft = df[((df['VUO'] == df.loc[i, 'VUO']) & (df['Hoofdstuknummer'] == df.loc[i, 'Hoofdstuknummer']) & (df['Artikelnummer'] == df.loc[i, 'Artikelnummer']) & (df['Artikelonderdeelnummer'] == 0))]

            # Stel de ouder in, indien gevonden
            if dft is not None and dft.shape[0] > 0:
                df.loc[i, 'Parent'] = df.loc[dft.index.to_list()[0], 'Index']

        if logging.root.level == logging.DEBUG:
            df.to_csv(os.path.join(config.settings['outputdir'], 'debug', 'setParent.csv'))
    except Exception as e:
        logging.critical(f"Fout tijdens instellen ouder-kind relaties: {str(e)}")
    logging.debug("Einde functie: setParent")
    return df['Parent']