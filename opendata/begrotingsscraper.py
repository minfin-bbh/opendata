#!/usr/bin/env python3

import pandas as pd
import requests
from bs4 import BeautifulSoup
import os
import shutil
import logging

input = r"o:\DGRB\Bz\BBH\Financieel management\Kwaliteit begroting en jaarverslag\De digitale begroting\Chaos op de N-schijf\Witte stukken\OWB 2024"
files = r"c:\Users\Administrator\Documents\Python Scripts\Scraper\stukken"
output = r"c:\Users\Administrator\Documents\Pre-Prinsjes\supplement"

_hoofdstukToName = {'I': 'De Koning', 'II': 'Staten-Generaal', 'IIA': 'Staten-Generaal', 'IIB': 'Overige Hoge Colleges van Staat, Kabinetten en de ...', 'III': 'Algemene Zaken', 'IV': 'Koninkrijksrelaties', 'V': 'Buitenlandse Zaken', 'VI': 'Justitie en Veiligheid', 'VII': 'Binnenlandse Zaken en Koninkrijksrelaties', 'VIII': 'Onderwijs, Cultuur en Wetenschap', 'IX': 'Financiën en Nationale Schuld', 'IXA': 'Nationale Schuld (Transactiebasis)', 'IXB': 'Financiën', 'X': 'Defensie', 'XII': 'Infrastructuur en Waterstaat', 'XIII': 'Economische Zaken en Klimaat', 'XIV': 'Landbouw, Natuur en Voedselkwaliteit', 'XV': 'Sociale Zaken en Werkgelegenheid', 'XV-40': 'Sociale Zaken en Werkgelegenheid', 'XVI': 'Volksgezondheid, Welzijn en Sport', 'XVI-41': 'Volksgezondheid, Welzijn en Sport', 'XVII': 'Buitenlandse Handel en Ontwikkelingssamenwerking', 'XIX': 'Nationaal Groeifonds', 'A': 'Mobiliteitsfonds', 'B': 'Gemeentefonds', 'C': 'Provinciefonds', 'F': 'Diergezondheidsfonds', 'H': 'BES-fonds', 'J': 'Deltafonds', 'K': 'Defensiematerieelbegrotingsfonds', 'L': 'Nationaal Groeifonds', 'M': 'Klimaatfonds', 'N': 'Transitiefonds'}


jaar = "2024"
fase = "OWB"
#OWB, O1, PS, O2, JV

def fetchxml():
    # Haalt uit de witte stukken-mappen alleen de .xml-bestanden 
    dirlist = os.listdir(input)

    for subdir in dirlist:
        os.chdir(f"{input}\{subdir}")

        for item in os.listdir(os.getcwd()):
            if '-1.xml' in item:
                mypath = os.path.join(input,subdir,item)
                shutil.copy(mypath, files)
    return 0


def getpaths(directory):
    # Voor alle bestanden het volledige pad ophalen
    paths = []
    for item in os.listdir(directory):
        paths.append(os.path.join(directory, item))

    return paths




def xml_to_pandas(file):
    with open(file,'r') as f:
        data = f.read()
    
    soup = BeautifulSoup(data, "lxml")


    hoofdstuk = soup.begrotingshoofdstuk.get_text()

    # rowlist = []
    # for item in soup.find_all('table'): 
    #     rows = item.find_all('row')
    #     rowlist.append(rows)
    # output = []
    # for row in rowlist:
    #     for i in row:
    #         tabledata = i.find_all('al')
    #         data = [j.text for j in tabledata]
    #         output.append(data)

    rowlist = []
    for item in soup.find_all('table'): 
        rows = item.find_all('row')
        rowlist.append(rows)
    output = []
    for row in rowlist:
        for i in row:
            tabledata = i.find_all('al')

            data = [j.text for j in tabledata]
            if len(data) > 0: # Lege rijen overslaan
                if len(data[0]) < 3:   #Alleen rijen met artikelnummers (1-99) dubbelchecken. Dat filtert rijen als 'totaal' eruit
                    if len(data) < 5:   # Rijen waarin iets ontbreekt dubbelchecken

                        # Niet alle tabellen hebben in de xml hetzelfde aantal rijen. Daarom een dubbelcheck. 

                        results = (i.find_all('entry'))
                        check = []
                        for result in results: 
                            if 'colname' in result.attrs:
                                check.append(result.attrs['colname'])
                        colnum = len(check)
                        newdata = data[:2]

                        v = i.find('entry', {"colname":f"col{colnum-2}"}).text
                        u = i.find('entry', {"colname":f"col{colnum-1}"}).text
                        o = i.find('entry', {"colname":f"col{colnum}"}).text

                        counter = 0
                        for item in [v, u, o]:
                            counter += 1
                            if item == '':
                                newdata.append('0')
                            else:
                                newdata.append(item)
                            if hoofdstuk == 'XVII':

                                pass#print(newdata)
                        data = newdata

            output.append(data)
            
        
       
        if hoofdstuk == "XVII":
            pass
            #print(output)




    #Standaardoperatie: kolomtitels opschonen
    #RISICO: je gaat ervanuit dat dit helemaal uniform is. Checken!
    
    output[0]=['Artikel', 'Omschrijving', 'Verplichtingen', 'Uitgaven', 'Ontvangsten']
    
    if output[1]==['Verplichtingen','Uitgaven','Ontvangsten']:
        del(output[1])
    else:
        logging.warning("Fout: tweede kolom niet zoals verwacht")

    #Standaardoperatie2 (2 in 1): lege rijen verwijderen, en tussenkopje 'Totaal' opschonen
    clean = []
    for item in output: 
        if item == []:
            pass
        elif item[0].lower()=="totaal":
            item.insert(0,"666")
            # Niet vanwege Satan, maar omdat het extreem onwaarschijnlijk is dat iemand ooit dit artikelnummer 'echt' zou invoeren in IBOS. 
            clean.append(item)
        elif len(item[0])>3:
            logging.info(f"Hoofdstuk {hoofdstuk} heeft afwijkende rij {item}")
        else:
            clean.append(item)          

    #Dataframe maken
    df = pd.DataFrame(clean, columns = output[0])
    #Eerste kolom droppen (zijn kolomnamen)
    #df.drop(index=0, inplace=True)

    df.reset_index(inplace=True, drop = True)

    # Hoofdstuknummer toevoegen
    df['Hoofdstuk']=hoofdstuk

    # Hoofdstukken opschonen om BES-fonds en Dierbeschermingsfonds apart te houden
    ## BES-fonds
    if hoofdstuk == "IV":
        chapters = []
        for i in range(len(df)):
            if not df['Omschrijving'].iloc[i] == "BES-fonds":
                chapters.append(df['Hoofdstuk'].iloc[i])
            else:
                chapters.append("H")
        df['Hoofdstuk'] = chapters

    ## Dierberschermingsfonds
    if hoofdstuk == "XIV":
        chapters = []
        for i in range(len(df)):
            if '1' in df['Artikel'].iloc[i] and 'dierziekten' in df['Omschrijving'].iloc[i].lower():
                chapters.append("F")
            else: 
                chapters.append(df['Hoofdstuk'].iloc[i])
        df['Hoofdstuk'] = chapters
            



    # Begrotingsjaar toevoegen
    df['Begrotingsjaar']=jaar



    # In vorm krijgen
    cols = df.columns.to_list()
    cols.remove('Omschrijving')
    cols.remove('Hoofdstuk') #Alleen numerieke kolommen
    
    for col in cols: 

        df[col]=df[col].str.replace('â€’','-', regex=True) #Bizarro-karakters voor min worden een 'gewone' min (UTF-8: U+002D)
        df[col]=df[col].str.replace('[^0-9-]+','', regex=True).astype(int, errors='ignore')
        #Alles dat geen cijfer of - is eruit halen. Dit schoont zowel punten op (1.000.000) als spaties (1 000 000) als bizarre metacharacters
    

    # Standen 'totaal' weghalen (die standen zijn niet artikelniveau, maar hoofdstukniveau)
    df = df[df['Artikel']!=666]


    #VUO splitsen (en kolommen her-ordenen)
    newdf = []
    
    for i in range(len(df)):
        row_v = []
        row_v.append(df.iloc[i]['Begrotingsjaar'])
        row_v.append("V")   #Waarde voor VUO-kolom invoegen
        row_v.append(df.iloc[i]['Hoofdstuk'])
        row_v.append(_hoofdstukToName[df.iloc[i]['Hoofdstuk']])
        row_v.append(df.iloc[i]['Artikel'])
        row_v.append(df.iloc[i]['Omschrijving'])
        row_v.append(df.iloc[i]['Verplichtingen'])
        newdf.append(row_v)

        row_u = []
        row_u.append(df.iloc[i]['Begrotingsjaar'])
        row_u.append("U")   #Waarde voor VUO-kolom invoegen
        row_u.append(df.iloc[i]['Hoofdstuk'])
        row_u.append(_hoofdstukToName[df.iloc[i]['Hoofdstuk']])
        row_u.append(df.iloc[i]['Artikel'])
        row_u.append(df.iloc[i]['Omschrijving'])
        row_u.append(df.iloc[i]['Uitgaven'])
        newdf.append(row_u)

        row_o = []
        row_o.append(df.iloc[i]['Begrotingsjaar'])
        row_o.append("O")   #Waarde voor VUO-kolom invoegen
        row_o.append(df.iloc[i]['Hoofdstuk'])
        row_o.append(_hoofdstukToName[df.iloc[i]['Hoofdstuk']])
        row_o.append(df.iloc[i]['Artikel'])
        row_o.append(df.iloc[i]['Omschrijving'])
        row_o.append(df.iloc[i]['Ontvangsten'])
        newdf.append(row_o)
    
    data = pd.DataFrame(newdf, columns = ['Begrotingsjaar', 'VUO', 'Hoofdstuknummer', 'Hoofdstuknaam', 'Artikelnummer', 'Artikelnaam', f"Stand{fase}"])
    #Lege rijen invullen met '0'
    data = data.fillna(0)

    # Voor hoofdstuk III de artikelnummers goed krijgen
    if hoofdstuk != "III":
        pass

    else: 
        for i in range(len(data)):

            if data.at[i, 'Artikelnaam'].lower().split()[0] == "eenheid":
                data.at[i, 'Hoofdstuknummer'] = 'IIIA'
            elif data.at[i, 'Artikelnaam'].lower().split()[0] == "kabinet":
                data.at[i, 'Hoofdstuknummer'] = 'IIIB'
            elif data.at[i, 'Artikelnaam'].lower().split()[0] == 'commissie':
                data.at[i, 'Hoofdstuknummer'] = 'IIIC'

            elif data.at[i, 'Artikelnaam'].lower() == 'totaal':
                pass
            else:
                logging.CRITICAL("Fout in instellen artikelnummers hoofdstuk III: onverwachte input")
    return data


def multiplexml(xmllist):
    df = pd.DataFrame()
    
    for item in xmllist:
        table = xml_to_pandas(item)
        df = pd.concat([df,table], axis = 0)
    df.reset_index(inplace=True, drop = True)

    return df








content = multiplexml(getpaths(files))
filename = os.path.join(output, f"{jaar}_{fase}_wittestukken.csv" )
content.to_csv(filename, index = False)
