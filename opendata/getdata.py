#!/usr/bin/env python3
# =============================================================================
__author__ = 'Rik Peters'
__copyright__ = 'Copyright 2023, Ministerie van Financiën'
__license__ = 'CC-BY'
__version__ = '0.2.0'
__maintainer__ = 'Rik Peters'
__email__ = 'r.j.peters@minfin.nl'
__status__ = 'Prototype'
# =============================================================================
"""Getdata: script om data te verzamelen"""
# =============================================================================
import opendata.config as config
from opendata.datacleaning import checkInternalConsistency
from opendata.datacleaning import cleanInputData
import logging
import pandas as pd
import warnings
warnings.filterwarnings("error")
from sqlalchemy import create_engine
from sqlalchemy.engine.url import URL
import requests
import os
# =============================================================================

def getdata(fase=None, inputstyle='dh', cumulatief=False, inputdata=None):
    if 'input' in config.settings['program']:
        owbcols = ['OWBMin2', 'OWBMin1', 'StandOWB', 'OWBPlus1', 'OWBPlus2', 'OWBPlus3', 'OWBPlus4']
        o1cols = ['MutatieOWB', 'StandVB', 'MutatieO1', 'StandO1', 'MutatiePlus1', 'MutatiePlus2', 'MutatiePlus3', 'MutatiePlus4']
        pscols = ['StandO1enISB', 'MutatiePSupp', 'StandPSupp']
        o2cols = ['StandVO1', 'MutatieO2MJN', 'MutatieO2Overig', 'StandO2']
        jvcols = ['RealisatieMin4', 'RealisatieMin3', 'RealisatieMin2', 'RealisatieMin1', 'Realisatie', 'Verschil']
    
        if cumulatief == True:

            
            # Ontwerpbegroting
            if fase >= config.OWB:
                
                # Inlezen
                owbdata = readInputFile(config.OWB, inputstyle)
                owbdata = cleanInputData(owbdata, config.OWB)
                fasecols = owbcols
                
                # Samenvoegen
                dataset = owbdata.copy()

                # Velden zetten
                dataset['Index'] = 'Nieuw in OWB'

                # Checks uitvoeren
                checkInternalConsistency(dataset, config.OWB)

            # Eerste supp
            if fase >= config.O1:
                fasecols.extend(o1cols)

                # Inlezen
                o1data = readInputFile(config.O1, inputstyle)
                o1data = cleanInputData(o1data, config.O1)

                if config.settings['inputstyle'] == 'swr':
                    mergestyle = 'right'
                else:
                    mergestyle = 'outer'
                # Samenvoegen
                dataset = dataset.merge(o1data, 'outer', on=['Hoofdstuknummer', 'Artikelnummer', 'VUO', 'Artikelonderdeelnummer', 'Instrumentnummer', 'Detailnummer'], suffixes=('', '_o1'))
                #dataset = dataset.merge(o1data, mergestyle, on=['IBOSnummer', 'VUO'], suffixes=('', '_o1'))


                # Velden zetten
                dataset['Index'] = dataset['Index'].fillna('Nieuw in O1')
                dataset['Artikelnaam'] = dataset['Artikelnaam'].fillna(dataset['Artikelnaam_o1'])
                dataset['IBOSnummer'] = dataset['IBOSnummer'].fillna(dataset['IBOSnummer_o1'])
                dataset['Totaal'] = dataset['Totaal'].fillna(dataset['Totaal_o1'])
                #dataset['Omschrijving'] = dataset['Omschrijving'].fillna(dataset['Omschrijving_o1'])
                dataset['Begrotingsjaar'] = dataset['Begrotingsjaar'].fillna(dataset['Begrotingsjaar_o1'])

                # Checks uitvoeren
                checkInternalConsistency(dataset, config.O1)

            # P-Supp
            if fase >= config.PS:
                fasecols.extend(pscols)
                # Inlezen
                
                psdata = readInputFile(config.PS, inputstyle)
                psdata = cleanInputData(psdata, config.PS)
                # Samenvoegen
                if config.settings['inputstyle'] == 'swr':
                    mergestyle = 'right'
                else:
                    mergestyle = 'outer'
                dataset = dataset.merge(psdata, mergestyle, on=['Hoofdstuknummer', 'Artikelnummer', 'VUO', 'Artikelonderdeelnummer', 'Instrumentnummer', 'Detailnummer'], suffixes=('', '_ps'))
                #dataset = dataset.merge(psdata, mergestyle, on=['IBOSnummer', 'VUO'], suffixes=('', '_ps'))


                # Velden zetten
                dataset['Index'] = dataset['Index'].fillna('Nieuw in PS')
                dataset['Artikelnaam'] = dataset['Artikelnaam'].fillna(dataset['Artikelnaam_ps'])
                dataset['IBOSnummer'] = dataset['IBOSnummer'].fillna(dataset['IBOSnummer_ps'])
                dataset['Totaal'] = dataset['Totaal'].fillna(dataset['Totaal_ps'])
                #dataset['Omschrijving'] = dataset['Omschrijving'].fillna(dataset['Omschrijving_ps'])
                dataset['Begrotingsjaar'] = dataset['Begrotingsjaar'].fillna(dataset['Begrotingsjaar_ps'])

                # Checks uitvoeren
                checkInternalConsistency(dataset, config.PS)


            # Tweede supp
            if fase >= config.O2:
                fasecols.extend(o2cols)

                # Inlezen
                o2data = readInputFile(config.O2, inputstyle)
                o2data = cleanInputData(o2data, config.O2)

                # Samenvoegen
                #dataset = dataset.merge(o2data, 'outer', on=['Hoofdstuknummer', 'Artikelnummer', 'VUO', 'Artikelonderdeelnummer', 'Instrumentnummer', 'Detailnummer'], suffixes=('', '_o2'))
                dataset = dataset.merge(o2data, 'outer', on=['IBOSnummer', 'VUO'], suffixes=('', '_o2'))

                # Velden zetten
                dataset['Index'] = dataset['Index'].fillna('Nieuw in O2')
                dataset['Artikelnaam'] = dataset['Artikelnaam'].fillna(dataset['Artikelnaam_o2'])
                dataset['IBOSnummer'] = dataset['IBOSnummer'].fillna(dataset['IBOSnummer_o2'])
                dataset['Totaal'] = dataset['Totaal'].fillna(dataset['Totaal_o2'])
                dataset['Omschrijving'] = dataset['Omschrijving'].fillna(dataset['Omschrijving_o2'])
                dataset['Begrotingsjaar'] = dataset['Begrotingsjaar'].fillna(dataset['Begrotingsjaar_o2'])

                # Checks uitvoeren
                checkInternalConsistency(dataset, config.O2)

            # Jaarverslag
            if fase >= config.JV:
                fasecols.extend(jvcols)
                # Inlezen
                jvdata = readInputFile(config.JV, inputstyle)
                jvdata = cleanInputData(jvdata, config.JV)

                # Samenvoegen
                dataset = dataset.merge(jvdata, 'outer', on=['Hoofdstuknummer', 'Artikelnummer', 'VUO', 'Artikelonderdeelnummer', 'Instrumentnummer', 'Detailnummer'], suffixes=('', '_jv'))

                # Velden zetten
                dataset['Index'] = dataset['Index'].fillna('Nieuw in JV')
                dataset['Artikelnaam'] = dataset['Artikelnaam'].fillna(dataset['Artikelnaam_jv'])
                dataset['IBOSnummer'] = dataset['IBOSnummer'].fillna(dataset['IBOSnummer_jv'])
                dataset['Totaal'] = dataset['Totaal'].fillna(dataset['Totaal_jv'])
                dataset['Omschrijving'] = dataset['Omschrijving'].fillna(dataset['Omschrijving_jv'])
                dataset['Begrotingsjaar'] = dataset['Begrotingsjaar'].fillna(dataset['Begrotingsjaar_jv'])

                # Checks uitvoeren
                checkInternalConsistency(dataset, config.JV)


  



        elif cumulatief == False:
            dataset = inputdata
            if config._faseToName[fase] == 'OWB':
                fasecols = owbcols
            elif config._faseToName[fase] == 'O1':
                fasecols = o1cols
            elif config._faseToName[fase] == 'PS':
                fasecols = pscols
            elif config._faseToName[fase] == 'O2':
                fasecols = o2cols
            elif config._faseToName[fase] == 'JV':
                fasecols = jvcols


        # Operaties over alle cols
        columns = ['Index', 'Parent', 'IBOSnummer', 'VUO', 'Totaal', 'Begrotingsjaar', 'Hoofdstuknummer', 'Hoofdstuknaam', 'Artikelnummer', 'Artikelnaam', 'Artikelonderdeelnummer', 'Artikelonderdeelnaam', 'Instrumentnummer', 'Instrumentnaam', 'Detailnummer', 'Detailnaam']
        columns.extend(fasecols)
        
        dataset = dataset[columns] # Drop alle tijdelijke kolommen
        dataset = dataset[dataset.columns[~dataset.columns.str.endswith(('_ibos', '_o1', '_ps', '_o2', '_jv'))]]
        
        fasecols.extend(['Begrotingsjaar', 'Artikelnummer', 'Artikelonderdeelnummer', 'Instrumentnummer', 'Detailnummer']) # intcols
        
        for col in fasecols:
            dataset[col] = dataset[col].fillna(0).astype(float) # Voorkom nan bij later toegevoegde regels
            try:
                dataset[col] = dataset[col].round(0).astype(str).str.split('.', expand=True)[0] # Verwijder pandas '.0'
            except:
                logging.debug(f"Could not remove '.0' in {col}")   


        return dataset