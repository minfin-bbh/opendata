#!/usr/bin/env python3
# =============================================================================
__author__ = 'Rik Peters'
__copyright__ = 'Copyright 2023, Ministerie van Financiën'
__license__ = 'CC-BY'
__version__ = '0.1.0'
__maintainer__ = 'Rik Peters'
__email__ = 'r.j.peters@minfin.nl'
__status__ = 'Prototype'
# =============================================================================
"""Script for consolidating output from the data hub"""
# =============================================================================
import pandas as pd
import os
import numpy as np
import time
import opendata.config as config
import logging
# =============================================================================




def consolidateDatahub(fase = None, jaar = config.settings['jaar']):
    logging.debug("Begin functie: consolidateDataHub")
    # ====== BEGIN ======

    # Naam van fase aanpassen aan model van datahub
    try: 
        fase = config._faseToName[fase]
    except Exception as e: 
        logging.info(f'Fase niet aangepast: {str(e)}. Fase blijft {fase}')

    if fase == "OWB":
        fase = "OW"
    if fase == "PS":
        fase = "I2"

 
    inputPath = os.path.join(config.settings['inputdir'], 'vies')
    outputPath = os.path.join(config.settings['outputdir'], 'medium')


### ======|| DOORLOPEN VAN BESTANDEN||====== ###
        
        # ====== DETAIL ======
        # Begin op detail-niveau, werk daarna omhoog
    try:
        # Ophalen
        niveau = 'Detail'   
        myPath = os.path.join(inputPath, f'OpenData-Btabel-{fase}-{niveau}.csv')
        df_d = pd.read_csv(myPath)
        
        # Bewerken
        df_d = df_d[df_d["VUO"]!="V"].copy() # Alle "V"-kolommen lager dan artikelniveau moeten weg.
        df_d = df_d[df_d['Detailnummer']!=0] # Alles met een detailnummer van 0 weg (is automatisch aangevuld door ibos)


        # ====== INSTRUMENT ======

        # Ophalen
        niveau = 'Instrument'
        myPath = os.path.join(inputPath, f'OpenData-Btabel-{fase}-{niveau}.csv')
        df_i = pd.read_csv(myPath)

        # Bewerken
        df_i = df_i[df_i["VUO"]!="V"] #Alle "V"-kolommen lager dan artikelniveau moeten weg.

        # Veranderen Totaal
        tot = []
        for i in range(len(df_i)):
            entry = 'N' #'Nee' tenzij match op lager niveau
            ibos = df_i['IBOSnummer'].iloc[i].split('.')
            vuo = df_i['VUO'].iloc[i]
            for j in range(len(df_d)):
                otheribos = df_d['IBOSnummer'].iloc[j].split('.')[:-1]
                othervuo = df_d['VUO'].iloc[j]
                if ibos == otheribos and vuo == othervuo:
                    entry = "J"
                    break # want 1 match is genoeg
            tot.append(entry)

        df_i['Totaal']=tot

        df_i.drop(df_i[(df_i['Totaal']=='N') & (df_i['Instrumentnummer']==0)].index, inplace = True)
        

        # ====== ARTIKELONDERDEEL ======

        # Ophalen
        niveau = 'ArtikelOnderdeel'
        myPath = os.path.join(inputPath, f'OpenData-Btabel-{fase}-{niveau}.csv')
        df_ao = pd.read_csv(myPath)

        # Bewerken
        df_ao = df_ao[df_ao["VUO"]!="V"] #Alle "V"-kolommen lager dan artikelniveau moeten weg.

        # Veranderen Totaal
        tot = []
        for i in range(len(df_ao)):
            entry = 'N' #'Nee' tenzij match op lager niveau
            ibos = df_ao['IBOSnummer'].iloc[i].split('.')
            vuo = df_ao['VUO'].iloc[i]
            for j in range(len(df_i)):
                otheribos = df_i['IBOSnummer'].iloc[j].split('.')[:-1]
                othervuo = df_i['VUO'].iloc[j]
                if ibos == otheribos and vuo == othervuo:
                    entry = "J"
                    break # want 1 match is genoeg
            tot.append(entry)
    
        df_ao['Totaal']=tot
        
        df_ao.drop(df_ao[(df_ao['Totaal']=='N') & (df_ao['Artikelonderdeelnummer']==0)].index, inplace = True)

        # ====== ARTIKEL ======

        # Ophalen
        niveau = 'Artikel'
        myPath = os.path.join(inputPath, f'OpenData-Btabel-{fase}-{niveau}.csv')
        df_a = pd.read_csv(myPath)

        # Bewerken

        # Veranderen Totaal
        tot = []
        for i in range(len(df_a)):
            entry = 'N' #'Nee' tenzij match op lager niveau
            ibos = df_a['IBOSnummer'].iloc[i].split('.')
            vuo = df_a['VUO'].iloc[i]
            for j in range(len(df_ao)):
                otheribos = df_ao['IBOSnummer'].iloc[j].split('.')[:-1]
                othervuo = df_ao['VUO'].iloc[j]
                if ibos == otheribos and vuo == othervuo:
                    entry = "J"
                    break # want 1 match is genoeg
            tot.append(entry)
    
        df_a['Totaal']=tot
        df_a.drop(df_a[(df_a['Totaal']=='N') & (df_a['Artikelnummer']==0)].index, inplace = True)



    ### ========= MERGE ======== ###
        dfs = [df_i, df_ao, df_a]
        data = df_d.copy()

        for item in dfs:
            data = data.merge(item, how = "outer")


        
    ### ======== LAATSTE OPERATIE =========

        '''Om consistent te zijn met input van eerdere jaren en met de pijplijn moeten tussenliggende nullen in IBOS weg
        Bijvoorbeeld: met detail 2023.1.1.0.1 correspondeert in de datahub instrument 1.1.0.
        Waar Totaal == 'J' en het laatste lid van IBOSnummer == 0 moet de rij verwijderd'''
    
        lastzero = []
        for i in range(len(data)):
            ibos = data['IBOSnummer'].iloc[i].split('.')
            if ibos[-1]=='0':
                lastzero.append("0")
            else:
                lastzero.append("1")
        data['lastzero'] = lastzero

        data.drop(data[(data['Totaal']=="J") & (data['lastzero']=="0")].index, inplace = True)
        data.drop(columns = ['lastzero'], inplace = True)

    ### ==== (Backup-)output ====
    
        if logging.root.level == logging.DEBUG:
            data.to_csv(os.path.join(config.settings['outputdir'], 'debug', 'consolidateDatahub.csv'), index = False)

    except Exception as e:
        logging.critical(f"Fout tijdens combineren datahub-bestanden: {str(e)}")
    
    logging.debug("Einde functie: consolidateDataHub")
    
    return data

def cleanDatahub(inputdata):
    ### ========= EXTRA BEWERKINGEN: DOUBLURES BINNEN DATAHUB VERWIJDEREN ======== ###
    ### (Bijvoorbeeld dubbele IBOS-nummers door spelfouten of veranderde namen)

    logging.debug("Begin functie: cleanDataHub")
    data = inputdata.copy()

    fase = config._faseToName[config.settings['fase']]
    if fase == 'PS':
        data.rename(columns={'StandVB': 'StandO1enISB', 'I2mutaties': 'MutatiePSupp', 'StandI2':'StandPSupp'}, inplace =True)
        # De pipeline gebruikt andere namen voor deze kolommen. We kunnen de query voor de datahub nog aanpassen om het goed te krijgen.


    ### ====== Dubbele IBOS-nummers (met beleid) verwijderen ====== ###

    if fase == "OWB" or fase == "JV":

        # Kolommen waarop straks bewerkingen worden uitgevoerd
        if fase == "OWB":
            numbercols = config.owbcols
        if fase ==  "JV":
            numbercols = config.jvcols

        # Dataframe met alleen de probleemgevallen (doublures)
        doubles = data.duplicated(subset=['IBOSnummer', 'VUO'], keep=False)
        data['doubles']=doubles
        nodoubles = data[data['doubles']==False]
        df2=data[data['doubles']==True]
        
        
        # Uitscheiden in schone basisset (yesdata) en vieze set die daarin geintegreerd moet (nodata)
        yeslist = []
        nolist = []
        
        for i in range(len(df2)):
            if not pd.isnull(df2['StandOWB'].iloc[i]):
                yeslist.append(df2.iloc[i])
            if pd.isnull(df2['StandOWB'].iloc[i]):
                nolist.append(df2.iloc[i])
        
        yesdata = pd.DataFrame(yeslist)
        nodata = pd.DataFrame(nolist)
        nodata.fillna(0, inplace = True)

        # Extra check: in de 'nodata' zitten ook entries die geen doublure van een entry in yesdata zijn
        # Bijvoorbeeld bij kostenposten die nu niet meer bestaan. Wel een stand op OWBMin1, geen IBOS-nummer dat correspondeert met een IBOS-nummer in huidige OWB
        
        extralist = []
        
        checker = yesdata['IBOSnummer'].to_list()
        for i in range(len(nodata)):
            if not nodata['IBOSnummer'].iloc[i] in checker:
                # DWZ: een IBOSnummer zonder stand in T=0, waarvan ook de doublure geen stand heeft op T=0.
                extralist.append(nodata.iloc[i])
        extradata = pd.DataFrame(extralist)
        #extradata.to_excel(f"{outputPath}/extra.xlsx", index = False)

        # extradata uitsplitsen in basisset (yes) en doublures (no)
        # NB: de doublures zelf zitten al in 'nodata'
        extradata['doubles'] = extradata.duplicated(subset=['IBOSnummer', 'VUO'])
        yesextradata = extradata[extradata['doubles']==False].copy()
        

        # de kolom 'doubles' kan verderop problemen veroorzaken
        yesdata.drop(columns=['doubles'], inplace = True)
        yesextradata.drop(columns=['doubles'], inplace = True)
        nodata.drop(columns=['doubles'], inplace = True)

        # Uitgesplitste data toevoegen aan yesdata, om mee te nemen bij verdere bewerkingen
        yesdata = pd.concat([yesdata, yesextradata], axis = 0).copy()

        # en deze 'yesextradata' wegfilteren uit 'nodata' om dubbeltelling te voorkomen

        nodata = pd.concat([nodata, yesextradata]).drop_duplicates(keep=False).copy()
     



        # De eigenlijke bewerkingen

        # Alle numerieke columns langsgaan.
        for column in numbercols: 
            newitems = []
            for i in range(len(yesdata)):
                
                counter = yesdata[column].iloc[i]
                ibos = yesdata['IBOSnummer'].iloc[i]
                vuo = yesdata['VUO'].iloc[i]

                for j in range(len(nodata)):
                    if not nodata['IBOSnummer'].iloc[j]==ibos:
                        pass
                    else:
                        if nodata['VUO'].iloc[j]==vuo:
                            counter = counter + nodata[column].iloc[j]
                
                newitems.append(counter)

            yesdata[column] = newitems

                
        # Fill remaining NaNs
        numdf = yesdata[numbercols].copy()
        other = yesdata.drop(columns = numbercols)

        numdf.fillna(0, inplace = True)

        cleandata = pd.concat([other, numdf], axis = 1)


        # En weer combineren met de niet-problematische data
        data = pd.concat([nodoubles, cleandata], axis = 0)
        data.drop(columns = ['doubles'], inplace = True)

        
        
    else: 
        pass
        # De bovenstaande operaties zijn voor Suppletoire begrotingen niet nodig - daar zijn geen historische standen in opgenomen.
        #TODO: versie maken die ook op jaarverslag werkt (daar zijn wel eerdere jaren in meegenomen)

    # Soms krijgen we een aanlevering via de swr die ook uit de datahub draait. Dan krijgt de swr voorrang, dus verwijderen we de relevante hoofdstukken uit de tabel van de datahub.
    filterlist = ['IXA', 'IXB', 'L' ,'XII']
    
    # Wegfilteren wat ook al in de swr zit
    for item in filterlist: 
        data = data[data['Hoofdstuknummer']!=item].copy()

    # Hoofdstuk 41 is een ander verhaal:
    if config.settings['fase'] == "PS":
        
        data = data[ data['IBOSnummer'].str.contains( f'{jaar}.41' )==False ].copy()
        # Voor PS is h41 aangeleverd via swr

    # Hoofdstuk 3 hoeft geen ABC bij:
    data['Hoofdstuknummer'].replace({'IIIA': 'III', 'IIIB': 'III', 'IIIC': 'III'}, inplace=True)


    # Voor verderop in de pijplijn (setIndex) is het belangrijk dat de hoofdstuknummers numeriek zijn: 
    data['Hoofdstuknummer'] = data.apply(lambda x: config._hoofdstukToKey.get(x['Hoofdstuknummer']), axis=1) # Label op hoofdstuk ipv numeriek


    # NaNs in artikelonderdeelnummer, instrumentnummer en detailnummer opschonen
    
    ibosonderdeelnummers = ['Artikelonderdeelnummer', 'Instrumentnummer', 'Detailnummer']
    for item in ibosonderdeelnummers:
        data[item].fillna(0, inplace=True)


### ===== OUTPUT ===== ###
 
    #fase juiste naam geven voor output       
    logging.debug("Einde functie: cleanDataHub")

    return data


def main():
    data = pd.read_csv(r"")
    cleanDatahub(data)
    return 0

if __name__ == "__main__":
    main()