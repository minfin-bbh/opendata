#!/usr/bin/env python3
# =============================================================================
__author__ = 'Christian C. Schouten'
__copyright__ = 'Copyright 2022, Ministerie van Financiën'
__license__ = 'CC-BY'
__version__ = '0.2.0'
__maintainer__ = 'Rik Peters'
__email__ = 'r.j.peters@minfin.nl'
__status__ = 'Prototype'
# =============================================================================
"""Source file for the data input functions"""
# =============================================================================
import opendata.config as config
import logging
import pandas as pd
import warnings
warnings.filterwarnings("error")
from sqlalchemy import create_engine
from sqlalchemy.engine.url import URL
import requests
import os
# =============================================================================

# Lees het inputbestand in voor een jaar/fase
def readInputFile(fase=None, inputstyle='dh'):
    logging.debug("Begin functie: readInputFile")
    inputdir = config.settings['inputdir']
    jaar = config.settings['jaar']
    if fase == None: fase = config.settings['fase']

    # In te lezen kolommen
    if inputstyle == 'dh':  
        columns=['Artikelnaam', 'IBOSnummer', 'VUO', 'Totaal', 'Artikelonderdeelnaam', 'Instrumentnaam', 'Detailnaam']
    if inputstyle == 'swr':
        columns=['Artikelnaam', 'IBOSnummer', 'VUO', 'Totaal', 'Omschrijving']

    if fase == config.OWB:  columns.extend(config.owbcols)
    elif fase == config.O1: columns.extend(config.o1cols)
    elif fase == config.PS: columns.extend(config.pscols)
    elif fase == config.O2: columns.extend(config.o2cols)
    elif fase == config.JV: columns.extend(config.jvcols)

    try:
        # Bouw bestandsnaam op
        logging.info(f"Start met inlezen bestand voor {config.settings['jaar']}/{config._faseToName.get(fase)}")
        
        fn = os.path.join(inputdir, inputstyle, str(jaar), f"{jaar}_{config._faseToName.get(fase)}.xlsx")
        # Lees excel in
        inputdata = pd.read_excel(fn, sheet_name=None) 
        # In een eerdere versie had de bovenstaande regel ook de optie 'names = columns' erbij. Zonder die optie neemt de functie ook input met een ander format aan (bijv. uit de datahub)
        inputdata = pd.concat(inputdata, ignore_index=True)

        logging.info(f"Klaar met inlezen van bestand")
        if logging.root.level == logging.DEBUG:
            inputdata.to_csv(os.path.join(config.settings['outputdir'], 'debug', 'readInputFile.csv'))
    except (ValueError, IndexError, Exception) as e:
        logging.critical(f"Fout tijdens inlezen bestand {fn}: {str(e)}")
    except RuntimeWarning as e:
        import ipdb; ipdb.set_trace()
        logging.error(f"Inputfout tijdens inlezen bestand {fn}: sheet heeft een header/footer ({str(e)})")
    logging.debug("Einde functie: readInputFile")
    return inputdata

# Haal de RIS/IBOS-standen op
def readIbosData():
    logging.debug("Begin functie: readIbosData")
    jaar = config.settings['jaar']
    fase = config._faseToName.get(config.settings['fase'])

    try:
        # Bouw query op
        qry = "SELECT BOL_ID, BEGROTINGSJAAR AS Begrotingsjaar, UITG_ONTV_IND AS VUO, BGG_NR AS Hoofdstuknummer, BGG_ROMEINS_NR AS HoofdstukRomeins, BEGROTING AS Hoofdstuknaam, A_NR AS Artikelnummer, ARTIKEL AS Artikelnaam, AO_NR AS Artikelonderdeelnummer, ARTIKELONDERDEEL AS Artikelonderdeelnaam, I_NR AS Instrumentnummer, INSTRUMENT AS Instrumentnaam, D_NR AS Detailnummer, DETAIL AS Detailnaam, "
        if fase == 'OWB': qry += "OW"
        elif fase == 'JV': qry += "SW"
        else: qry += fase
        qry += " AS ibos_bedrag FROM [RHBP].[WINFIN\\HALVEMAAN].ris_vw_begrotingsstaat_detail"
        if fase == 'OWB': qry += "_tp1" # Account for year shift
        selfrom = qry
        qry = f"{selfrom} WHERE BGG_NR != '16' AND BEGROTINGSJAAR = {jaar} UNION {selfrom}_RZ WHERE BGG_NR = '16' AND BEGROTINGSJAAR = {jaar}" # Alleen kaders R en Z (niet N) voor VWS
        logging.debug(f"IBOS query: {qry}")

        # Lees query uit
        #cnx = create_engine(URL.create("mssql+pyodbc", query={"odbc_connect": config.ibos_connstr}))
        #df = pd.read_sql(qry, cnx)
        df = pd.read_csv(os.path.join(config.settings['outputdir'], 'mock', 'ibosdata.csv'), sep=';', # mock ivm GAO
                dtype={'BOL_ID': 'Int64','Begrotingsjaar': 'Int64', 'VUO': str, 'Hoofdstuknummer': str, 'HoofdstukRomeins': str, 'Hoofdstuknaam': str, 'Artikelnummer': 'Int64', 'Artikelnaam': str, 'Artikelonderdeelnummer': 'Int64', 'Artikelonderdeelnaam': str, 'Instrumentnummer': 'Int64', 'Instrumentnaam': str, 'Detailnummer': 'Int64', 'Detailnaam': str, 'ibos_bedrag': 'Int64'})

        logging.info("Klaar met IBOS query")
        if logging.root.level == logging.DEBUG:
            df.to_csv(os.path.join(config.settings['outputdir'], 'debug', 'readIbosData.csv'))
    except Exception as e:
        logging.critical(f"Fout tijdens uitvoeren IBOS query: {str(e)}")
    logging.debug("Einde functie: readIbosData")
    return df

# Wrapper om de database heen
def executeApiCommand(command='R', endpoint='budgettairetabellen/UntypedDataSet', data=None):
    logging.debug("Begin functie: executeApiCommand")

    # Bouw API parameters op
    api_url = config.api_url + endpoint
    proxies = None
    if hasattr(config, 'proxies'): proxies = config.proxies
    cert_path = None
    if hasattr(config, 'cert_path'): cert_path = config.cert_path

    try:
        # Switch op command voor HTTP method
        if command == 'C': # Create
            r = requests.post(api_url, data=data, proxies=proxies, verify=cert_path)
        elif command == 'R': # Read
            r = requests.get(api_url, params=data, proxies=proxies, verify=cert_path)
        elif command == 'U': # Update
            r = requests.put(api_url, data=data, proxies=proxies, verify=cert_path)
        elif command == 'D': # Delete
            r = requests.delete(api_url, data=data, proxies=proxies, verify=cert_path)

        # Geef JSON terug
        r = r.json()

        logging.debug(f"Klaar met API call: commando {command}, data '{data}'")
    except Exception as e:
        logging.critical(f"Fout tijdens uitvoeren API call: commando {command}, url '{r.url}', data '{data}' ({str(e)})")
    logging.debug("Einde functie: executeApiCommand")
    return r

# Haal de volledige dataset uit de API
def readCompleteDataSet():
    logging.debug("Begin functie: readCompleteDataSet")
    df = None

    try:
        # Default endpoint, geen payload
        r = executeApiCommand('R')
        df = pd.DataFrame(r['value'])

        # Filter onnodige kolommen weg
        columns = ['Index', 'Parent', 'IBOSnummer', 'VUO', 'Totaal', 'Begrotingsjaar', 'Hoofdstuknummer', 'Hoofdstuknaam', 'Artikelnummer', 'Artikelnaam', 'Artikelonderdeelnummer', 'Artikelonderdeelnaam', 'Instrumentnummer', 'Instrumentnaam', 'Detailnummer', 'Detailnaam', 'OWBMin2', 'OWBMin1', 'StandOWB', 'OWBPlus1', 'OWBPlus2', 'OWBPlus3', 'OWBPlus4', 'MutatieOWB', 'StandVB', 'MutatieO1', 'StandO1', 'MutatiePlus1', 'MutatiePlus2', 'MutatiePlus3', 'MutatiePlus4', 'StandVO1', 'MutatieO2MJN', 'MutatieO2Overig', 'StandO2'] #, 'RealisatieMin4', 'RealisatieMin3', 'RealisatieMin2', 'RealisatieMin1', 'Realisatie', 'Verschil']
        df = df[columns]

        logging.info("Bestaande dataset succesvol ingelezen")
        if logging.root.level == logging.DEBUG:
            df.to_csv(os.path.join(config.settings['outputdir'], 'debug', 'readCompleteDataSet.csv'))
    except Exception as e:
        logging.critical(f"Fout tijdens uitvoeren API call budgettairetabellen: {str(e)}")
    logging.debug("Einde functie: readCompleteDataSet")
    return df



