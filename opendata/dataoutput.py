#!/usr/bin/env python3
# =============================================================================
__author__ = 'Christian C. Schouten'
__copyright__ = 'Copyright 2022, Ministerie van Financiën'
__license__ = 'CC-BY'
__version__ = '0.2.0'
__maintainer__ = 'Rik Peters'
__email__ = 'r.j.peters@minfin.nl'
__status__ = 'Prototype'
# =============================================================================
"""Source file for the data output functions"""
# =============================================================================
import opendata.config as config
from opendata.datainput import executeApiCommand
import logging
import pandas as pd
from pyexcelerate import Workbook
from pyexcel_ods import save_data
from collections import OrderedDict
import os
# =============================================================================

# Schrijf nieuwe regels terug naar de database
def writeDataChanges(inputdata):
    logging.debug("Begin functie: writeDataChanges")

    try:
        # Verwijderen eventuele oude regels
        r = executeApiCommand('R', data={'begrotingsjaar': config.settings['jaar']})
        df = pd.DataFrame(r['value'])
        if df.shape[0] > 1:
            executeApiCommand('D', data={'Index': df['Index'].tolist()})
            logging.info(f"Klaar met verwijderen {df.shape[0]} oude regels")
        if logging.root.level == logging.DEBUG:
            df.to_csv(os.path.join(config.settings['outputdir'], 'debug', 'writeDataChanges_old.csv'))

        # Filter bijgewerkte regels en schrijf die terug
        df = inputdata[inputdata['Begrotingsjaar'] == config.settings['jaar']]
        df.apply(lambda x: executeApiCommand('C', data=x.to_dict()), axis=1)

        logging.info(f"Klaar met wegschrijven {df.shape[0]} nieuwe regels")
        if logging.root.level == logging.DEBUG:
            df.to_csv(os.path.join(config.settings['outputdir'], 'debug', 'writeDataChanges_new.csv'))
    except Exception as e:
        logging.critical(f"Fout tijdens wegschrijven nieuwe regels: {str(e)}")
    logging.debug("Einde functie: writeDataChanges")

# Exporteer de website-bestanden (donut en open data)
def exportWebsiteFiles(dataset):
    logging.debug("Begin functie: exportWebsiteFiles")
    if config.settings['premiefilter'] == False:
       
        exportDonutData(dataset) # Databestand donut
        exportOpenData(dataset)  # Open data-bestanden voor download

    else:
        exportDonutData(dataset, premies = True) # Databestand donut, inclusief premies
        exportDonutData(dataset, premies = False) # Databestand donut, exclusief premies

        exportOpenData(dataset, premies = True)
        exportOpenData(dataset, premies = False)

    logging.debug("Einde functie: exportWebsiteFiles")

# Exporteer het databestand voor de donut
def exportDonutData(inputdata, premies = False):
    logging.debug("Begin functie: exportDonutData")
    df = inputdata.copy()

    try:
        # Filter het gewenste jaar en beperk tot de N-regels
        df = df[df['Begrotingsjaar'].astype('int64') == int(config.settings['jaar'])]
        df = df[df['Totaal'] == 'N']

        # Filter de gewenste kolommen
        owbcols = ['OWBMin2', 'OWBMin1', 'StandOWB', 'OWBPlus1', 'OWBPlus2', 'OWBPlus3', 'OWBPlus4']
        o1cols = ['StandOWB', 'MutatieOWB', 'StandVB', 'MutatieO1', 'X7', 'StandO1', 'MutatiePlus1', 'MutatiePlus2', 'MutatiePlus3', 'MutatiePlus4']
        pscols = ['StandO1enISB', 'MutatiePSupp', 'StandPSupp']
        o2cols = ['StandVB', 'StandVO1', 'MutatieO2MJN', 'MutatieO2Overig', 'StandO2']
        jvcols = ['RealisatieMin4', 'RealisatieMin3', 'RealisatieMin2', 'RealisatieMin1', 'Realisatie']
        columns = ['Begrotingsjaar', 'Hoofdstuknummer', 'X1', 'Hoofdstuknaam', 'X2', 'Begrotingsfase', 'Artikelnummer', 'X3', 'Artikelnaam', 'X4', 'X5', 'X6', 'VUO', 'Totaal', 'IBOSnummer', 'Artikelonderdeelnaam', 'Artikelonderdeelnummer', 'Instrumentnaam', 'Instrumentnummer', 'Detailnaam', 'Detailnummer']
        if config.settings['fase'] == config.OWB: columns.extend(owbcols)
        if config.settings['fase'] == config.O1: columns.extend(o1cols)
        if config.settings['fase'] == config.PS: columns.extend(pscols)
        if config.settings['fase'] == config.O2: columns.extend(o2cols)
        if config.settings['fase'] == config.JV: columns.extend(jvcols)
        columns.extend(['Index'])

        # Tijdelijk in afwachting van aanpassing website
        for col in ['X1', 'X2', 'X3', 'X4', 'X5', 'X6']: df[col] = ''
        if config.settings['fase'] == config.O1: df['X7'] = ''
        df['Begrotingsfase'] = config._faseToName.get(config.settings['fase'])
        df = df[columns]
    
        # Bouw bestandsnaam op en exporteer naar CSV
        if config.settings['premiefilter']==False:
            filename = os.path.join(config.settings['outputdir'], f"Donut_{config.settings['jaar']}_{config._faseToName.get(config.settings['fase'])}.csv")
            df.to_csv(filename, index=False)
        else:
            if premies == True:
                filename = os.path.join(config.settings['outputdir'], f"Donut_{config.settings['jaar']}_{config._faseToName.get(config.settings['fase'])}_incl.csv")
            if premies == False:
                filename = os.path.join(config.settings['outputdir'], f"Donut_{config.settings['jaar']}_{config._faseToName.get(config.settings['fase'])}_excl.csv")

            df.to_csv(filename, index=False) 

        logging.info("Databestand voor de website succesvol geëxporteerd")
    except Exception as e:
        logging.critical(f"Fout tijdens exporteren databestand website: {str(e)}")
    logging.debug("Einde functie: exportDonutData")

# Exporteer de open data-bestanden
def exportOpenData(inputdata, premies = False):
    logging.debug("Begin functie: exportOpenData")
    df = inputdata.copy()

    try:
        # Filter out desired columns
        columns = ['Index', 'Parent', 'IBOSnummer', 'VUO', 'Totaal', 'Begrotingsjaar', 'Hoofdstuknummer', 'Hoofdstuknaam', 'Artikelnummer', 'Artikelnaam', 'Artikelonderdeelnummer', 'Artikelonderdeelnaam', 'Instrumentnummer', 'Instrumentnaam', 'Detailnummer', 'Detailnaam', 'OWBMin2', 'OWBMin1', 'StandOWB', 'OWBPlus1', 'OWBPlus2', 'OWBPlus3', 'OWBPlus4', 'MutatieOWB', 'StandVB', 'MutatieO1', 'StandO1', 'MutatiePlus1', 'MutatiePlus2', 'MutatiePlus3', 'MutatiePlus4', 'StandVO1', 'MutatieO2MJN', 'MutatieO2Overig', 'StandO2', 'RealisatieMin4', 'RealisatieMin3', 'RealisatieMin2', 'RealisatieMin1', 'Realisatie', 'Verschil']
        df = df.reindex(columns=columns)

        # Bouw bestandsnaam op
        if config.settings['premiefilter']==False:
            filename = os.path.join(config.settings['outputdir'], f"Budgettaire Tabellen {df['Begrotingsjaar'].astype(int).min()}-{df['Begrotingsjaar'].astype(int).max()}")

        else: 
            if premies == False:
                filename = os.path.join(config.settings['outputdir'], f"Budgettaire Tabellen {df['Begrotingsjaar'].astype(int).min()}-{df['Begrotingsjaar'].astype(int).max()}_excl")
            if premies == True:
                filename = os.path.join(config.settings['outputdir'], f"Budgettaire Tabellen {df['Begrotingsjaar'].astype(int).min()}-{df['Begrotingsjaar'].astype(int).max()}_incl")

        # Exporteer naar XLSX, ODS, CSV en JSON
        df.to_csv(filename+'.csv', index=False)
        df.to_json(filename+'.json', index=False, orient='table')
        #df.to_excel(filename+'.xlsx', index=False) # too slow...
        wb = Workbook()
        xldf = df.copy() # Extra kopie om in excel NaNs beter weer te geven
        xldf.fillna('', inplace = True)
        data = [xldf.columns.tolist(), ] + xldf.values.tolist()
        wb.new_sheet('Data', data=data)
        wb.save(filename+'.xlsx')
        #df.to_excel(filename+'.ods', index=False, engine='odf') # too slow...
        df = df.astype(str)
        d = OrderedDict()
        d.update({"Data": data})
        save_data(filename+'.ods', d)

        logging.info("Open data-bestanden voor de website succesvol geëxporteerd")
    except Exception as e:
        logging.critical(f"Fout tijdens exporteren open data-bestanden: {str(e)}")
    logging.debug("Einde functie: exportOpenData")
