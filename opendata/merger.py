#!/usr/bin/env python3

# =============================================================================
__author__ = 'Rik Peters'
__copyright__ = 'Copyright 2023, Ministerie van Financiën'
__license__ = 'CC-BY'
__version__ = '0.1.0'
__maintainer__ = 'Rik Peters'
__email__ = 'r.j.peters@minfin.nl'
__status__ = 'Prototype'
# =============================================================================
"""Script to merge files from datahub with files from SWR (after cleaning)"""
# =============================================================================
import pandas as pd
import os
import opendata.config as config
import logging


year = config.settings['jaar']
fase = config._faseToName[config.settings['fase']]

inputPath = config.settings['inputdir']
outputPath = config.settings['outputdir']



def normalizeIbos(inputdata):
    df = inputdata.copy()

    try:
        ibos = df['IBOSnummer'].tolist()
        output = []

        for item in ibos: 
            dummy = item.split('.')
            newdummy = ''
            for subitem in dummy:
                if len(subitem) == 4:
                    newdummy = newdummy + subitem   #voor jaartallen (eerste deel IBOS-nummer)
                else: 
                    if subitem=='0':
                        newdummy = newdummy + '.' + '0'
                    elif subitem[0]=='0':
                        newdummy = newdummy + '.' + str(subitem[1])
                    else: 
                        newdummy = newdummy + '.' + str(subitem)
            output.append(newdummy)

        df['IBOSnummer'] = output
    except Exception as e:
        logging.CRITICAL(f"IBOS normaliseren mislukt: {str(e)}")

    return df


def merge(swr=None, dh=None, fase = config._faseToName[config.settings['fase']]):
    logging.info("Begin functie: merge")
    if swr == None:
        swr = pd.read_csv(f'{inputPath}/medium/{year}_{fase}_swr.csv')
    if dh == None:
        dh = pd.read_csv(f'{inputPath}/medium/{year}_{fase}_dh.csv')

    if fase == 'PS':
        dh.rename(columns={"StandVB":"StandO1enISB", "I2mutaties":"MutatiePSupp", "StandI2": "StandPSupp"}, inplace = True)
        swr.rename(columns={"StandVB":"StandO1enISB", "I2mutaties":"MutatiePSupp", "StandI2": "StandPSupp"}, inplace = True)

    # Add tags to both dfs to mark their origin
    #swr.drop(columns=['Index', 'Parent']) #Worden gegenereerd door de pijplijn, zijn pas voor het totaalbestand nodig
    swr['origin']='swr'
    dh['origin']='dh'

    swr = normalizeIbos(swr)
    dh = normalizeIbos(dh)
    # Filter chapter III from datahub

    try:
        dh = dh[dh['Hoofdstuknummer']!='IIIA']
        dh = dh[dh['Hoofdstuknummer']!='IIIB']
        dh = dh[dh['Hoofdstuknummer']!='IIIC']
    except: 
        logging.CRITICAL("Error in filtering out chapter III")



    # check if there are any double entries
    df = pd.concat([swr, dh], axis = 0)
        # De volgorde hier bepaalt welke doublures er precies uitgaan. De eerste overtoept de tweede.

    doubles = df.duplicated(subset=['IBOSnummer', 'VUO'], keep=False)
    df['doubles'] = doubles
    df2 = df[df['doubles']==True].copy()

    # Sanity check: are all the doubles due to double input from swr & datahub? If so, proceed. If not, stop.
    swrlength = len(df2[df2['origin']=='swr'])
    dhlength = len(df2[df2['origin']=='dh'])
    if not swrlength == dhlength:   
        logging.CRITICAL(f"Critical error: not all doubles are due to double input. The swr gives {swrlength}, the datahub gives {dhlength}")
        exit()

        

    # TODO: Extra sanity check: make sure the identical length of the doubles is not a freak accident. There should be NO columns from the swr with doubles.


    else: 
        dropcols=['origin', 'doubles', 'Index', 'Parent']
        
        doubles2 = df.duplicated(subset=['IBOSnummer', 'VUO'], keep='first')
        if len(doubles2) !=0:
            df['delete'] = doubles2 
            data = df[~df['delete'] == True].copy()
            dropcols.append('delete')
        else:
            data = df.copy()


            
        ### ======== LAATSTE OPERATIE =========

            '''Om consistent te zijn met input van eerdere jaren en met de pijplijn moeten tussenliggende nullen in IBOS weg
            Bijvoorbeeld: met detail 2023.1.1.0.1 correspondeert in de datahub instrument 1.1.0.
            Waar Totaal == 'J' en het laatste lid van IBOSnummer == 0 moet de rij verwijderd'''
    if fase == 'PS':
        lastzero = []
        for i in range(len(data)):
            ibos = data['IBOSnummer'].iloc[i].split('.')
            if ibos[-1]=='0':
                lastzero.append("0")
            else:
                lastzero.append("1")
        data['lastzero'] = lastzero

        data.drop(data[(data['Totaal']=="J") & (data['lastzero']=="0")].index, inplace = True)
        data.drop(columns = ['lastzero'], inplace = True)

    # Remove temporary columns
    data.drop(columns=dropcols, inplace=True)

    if logging.root.level == logging.DEBUG:
        data.to_excel(f'{outputPath}/{year}_{fase}.xlsx', index = False)
    logging.info("Einde functie: merge")
    return data

def main():
    merge(fase)

if __name__ == "__main__":
    main()