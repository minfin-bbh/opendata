#!/usr/bin/env python3
# =============================================================================
__author__ = 'Christian C. Schouten'
__copyright__ = 'Copyright 2022, Ministerie van Financiën'
__license__ = 'CC-BY'
__version__ = '0.2.0'
__maintainer__ = 'Rik Peters'
__email__ = 'r.j.peters@minfin.nl'
__status__ = 'Prototype'
# =============================================================================
"""Source file for the __init__.py defining the opendata package"""
# =============================================================================
import opendata.datainput
import opendata.datacleaning
import opendata.config
import opendata.dataoutput
import opendata.budgetstatement
# =============================================================================
