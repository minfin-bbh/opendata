#!/usr/bin/env python3
# =============================================================================
__author__ = 'Christian C. Schouten'
__copyright__ = 'Copyright 2022, Ministerie van Financiën'
__license__ = 'CC-BY'
__version__ = '0.2.0'
__maintainer__ = 'Rik Peters'
__email__ = 'r.j.peters@minfin.nl'
__status__ = 'Prototype'
# =============================================================================
"""Source file for the budget statement functions"""
# =============================================================================
import opendata.config as config
from opendata.datainput import executeApiCommand
import logging
import pandas as pd
from sqlalchemy import create_engine
from sqlalchemy.engine.url import URL
import numpy as np
from pyexcelerate import Workbook
from pyexcel_ods import save_data
from collections import OrderedDict
import os
# =============================================================================

# Haal de begrotingsstaten uit IBOS
def loadBudgetData():
    logging.debug("Begin functie: loadBudgetData")

    # Ophalen en schoonmaken
    df = retrieveBudgetData()
    df = cleanBudgetData(df)
    
    # Detailniveau voor de AR
    retrieveBudgetDetailData()

    logging.debug("Einde functie: loadBudgetData")
    return df

# De begrotingsstaten worden als open data gepubliceerd op artikelniveau
def retrieveBudgetData():
    logging.debug("Begin functie: retrieveBudgetData")

    try:
        # Bouw de query op a.d.h.v. inputvariabelen
        jaar = config.settings['jaar']
        qry = "SELECT BOL_ID, BEGROTINGSJAAR AS Begrotingsjaar, UITG_ONTV_IND AS VUO, BGG_NR AS Hoofdstuknummer, BGG_ROMEINS_NR AS HoofdstukRomeins, BEGROTING AS Hoofdstuknaam, A_NR AS Artikelnummer, ARTIKEL AS Artikelnaam, OW AS StandOWB, VB AS StandVB, O1 AS StandO1, O2 AS StandO2, SW AS Realisatie FROM [RHBP].[WINFIN\\HALVEMAAN].ris_vw_begrotingsstaat_artikel WHERE BEGROTINGSJAAR BETWEEN 2013 AND "
        if config.settings['fase'] == config.OWB: 
            qry += f"{jaar-1} UNION SELECT BOL_ID, BEGROTINGSJAAR AS Begrotingsjaar, UITG_ONTV_IND AS VUO, BGG_NR AS Hoofdstuknummer, BGG_ROMEINS_NR AS HoofdstukRomeins, BEGROTING AS Hoofdstuknaam, A_NR AS Artikelnummer, ARTIKEL AS Artikelnaam, OW AS StandOWB, VB AS StandVB, O1 AS StandO1, O2 AS StandO2, SW AS Realisatie FROM [RHBP].[WINFIN\\HALVEMAAN].ris_vw_begrotingsstaat_artikel_tp1 WHERE BEGROTINGSJAAR = {jaar}"
        elif config.settings['fase'] == config.O1:
            qry += str(jaar)
        else:
            qry += str(jaar+1)
        logging.debug(f"IBOS query: {qry}")

        # Bereid dataframe voor
        #cnx = create_engine(URL.create("mssql+pyodbc", query={"odbc_connect": config.ibos_connstr}))
        #df = pd.read_sql(qry, cnx)
        df = pd.read_csv(os.path.join(config.settings['outputdir'], 'mock', 'bgg.csv'), sep=';', encoding='utf-8', # mock ivm GAO
                dtype={'BOL_ID': 'Int64', 'Begrotingsjaar': 'Int64', 'VUO': str, 'Hoofdstuknummer': str, 'HoofdstukRomeins': str, 'Hoofdstuknaam': str, 'Artikelnummer': 'Int64', 'Artikelnaam': str, 'StandOWB': 'Int64', 'StandVB': 'Int64', 'StandO1': 'Int64', 'StandO2': 'Int64', 'Realisatie': 'Int64'})

        logging.info("IBOS query begrotingsstaten succesvol uitgevoerd")
        if logging.root.level == logging.DEBUG:
            df.to_csv(os.path.join(config.settings['outputdir'], 'debug', 'retrieveBudgetData.csv'))
    except Exception as e:
        logging.critical(f"Fout tijdens uitvoeren IBOS query: {str(e)}")
    logging.debug("Einde functie: retrieveBudgetData")
    return df

# De AR krijgt een dump van de begrotingsstaten op detailniveau
def retrieveBudgetDetailData():
    logging.debug("Begin functie: retrieveBudgetDetailData")

    try:
        # Bouw de query op a.d.h.v. inputvariabelen
        jaar = config.settings['jaar']
        selfrom = "SELECT BOL_ID, BEGROTINGSJAAR AS Begrotingsjaar, UITG_ONTV_IND AS VUO, BGG_NR AS Hoofdstuknummer, BGG_ROMEINS_NR AS HoofdstukRomeins, BEGROTING AS Hoofdstuknaam, A_NR AS Artikelnummer, ARTIKEL AS Artikelnaam, AO_NR AS Artikelonderdeelnummer, ARTIKELONDERDEEL AS Artikelonderdeelnaam, I_NR AS Instrumentnummer, INSTRUMENT AS Instrumentnaam, D_NR AS Detailnummer, DETAIL AS Detailnaam, OW AS StandOWB, VB AS StandVB, O1 AS StandO1, O2 AS StandO2, SW AS Realisatie FROM [RHBP].[WINFIN\\HALVEMAAN].ris_vw_begrotingsstaat_detail"
        if config.settings['fase'] == config.OWB:
            qry = f"{selfrom} WHERE BGG_NR != '16' AND BEGROTINGSJAAR BETWEEN 2013 AND {jaar-1} UNION {selfrom}_tp1 WHERE BGG_NR != '16' AND BEGROTINGSJAAR = {jaar} UNION {selfrom}_RZ WHERE BGG_NR = '16' AND BEGROTINGSJAAR BETWEEN 2013 AND {jaar-1} UNION {selfrom}_tp1_RZ WHERE BGG_NR = '16' AND BEGROTINGSJAAR = {jaar}"
        elif config.settings['fase'] == config.O1:
            qry = f"{selfrom} WHERE BGG_NR != '16' AND BEGROTINGSJAAR BETWEEN 2013 AND {jaar} UNION {selfrom}_RZ WHERE BGG_NR = '16' AND BEGROTINGSJAAR BETWEEN 2013 AND {jaar}"
        else:
            qry = f"{selfrom} WHERE BGG_NR != '16' AND BEGROTINGSJAAR BETWEEN 2013 AND {jaar+1} UNION {selfrom}_RZ WHERE BGG_NR = '16' AND BEGROTINGSJAAR BETWEEN 2013 AND {jaar+1}"
        logging.debug(f"IBOS query: {qry}")

        # Bereid dataframe voor
        #cnx = create_engine(URL.create("mssql+pyodbc", query={"odbc_connect": config.ibos_connstr}))
        #df = pd.read_sql(qry, cnx)
        df = pd.read_csv(os.path.join(config.settings['outputdir'], 'mock', 'detail.csv'), sep=';', encoding='utf-8', # mock ivm GAO
                dtype={'BOL_ID': 'Int64','Begrotingsjaar': 'Int64', 'VUO': str, 'Hoofdstuknummer': str, 'HoofdstukRomeins': str, 'Hoofdstuknaam': str, 'Artikelnummer': 'Int64', 'Artikelnaam': str, 'Artikelonderdeelnummer': 'Int64', 'Artikelonderdeelnaam': str, 'Instrumentnummer': 'Int64', 'Instrumentnaam': str, 'Detailnummer': 'Int64', 'Detailnaam': str, 'StandOWB': 'Int64', 'StandVB': 'Int64', 'StandO1': 'Int64', 'StandO2': 'Int64', 'Realisatie': 'Int64'})

        # Schoonmaken en exporteren
        df = cleanBudgetData(df)
        df.to_csv(os.path.join(config.settings['outputdir'], f"Begrotingsstaten {jaar} (detail).csv"), index=False)

        logging.info("IBOS query begrotingsstaten detail succesvol uitgevoerd")
        if logging.root.level == logging.DEBUG:
            df.to_csv(os.path.join(config.settings['outputdir'], 'debug', 'retrieveBudgetDetailData.csv'))
    except Exception as e:
        logging.critical(f"Fout tijdens uitvoeren IBOS query: {str(e)}")
    logging.debug("Einde functie: retrieveBudgetDetailData")

# Data schoonmaken
def cleanBudgetData(inputdata):
    logging.debug("Begin functie: cleanBudgetData")
    df = inputdata.copy()
    fase = config.settings['fase']
    jaar = config.settings['jaar']

    try:
        # Drop tijdelijke kolommen
        df = df.drop(columns=['BOL_ID', 'Hoofdstuknummer'])

        # Zet toekomstige waarden op 0
        if fase == config.OWB:
            df = df[df['Begrotingsjaar'] <= jaar]   # Sep t-1
            df.loc[(df['Begrotingsjaar'] == jaar-1), ['StandO2', 'Realisatie']] = 0
            df.loc[(df['Begrotingsjaar'] == jaar), ['StandVB', 'StandO1', 'StandO2', 'Realisatie']] = 0
        elif fase == config.O1:
            df = df[df['Begrotingsjaar'] <= jaar]   # May t
            df.loc[(df['Begrotingsjaar'] == jaar), ['StandO2', 'Realisatie']] = 0
        elif fase == config.O2:
            df = df[df['Begrotingsjaar'] <= jaar+1] # Nov t
            df.loc[(df['Begrotingsjaar'] == jaar), 'Realisatie'] = 0
            df.loc[(df['Begrotingsjaar'] == jaar+1), ['StandO1', 'StandO2', 'Realisatie']] = 0
        elif fase == config.JV:
            df = df[df['Begrotingsjaar'] <= jaar+1] # May t+1
            df.loc[(df['Begrotingsjaar'] == jaar+1), ['StandO1', 'StandO2', 'Realisatie']] = 0

        # Schoonmaken
        df = df.replace({np.nan: 0})
        cols = ['Begrotingsjaar', 'Artikelnummer', 'StandOWB', 'StandVB', 'StandO1', 'StandO2', 'Realisatie']
        for col in cols:
            df[col] = df[col].round(0).astype(str).str.split('.', expand=True)[0] # Verwijder pandas '.0'

        logging.info("Begrotingsstaten succesvol schoongemaakt")
        if logging.root.level == logging.DEBUG:
            df.to_csv(os.path.join(config.settings['outputdir'], 'debug', 'cleanBudgetData.csv'))
    except Exception as e:
        logging.critical(f"Fout tijdens schoonmaken begrotingsstaten: {str(e)}")
    logging.debug("Einde functie: cleanBudgetData")
    return df

# Schrijf bijgewerkte rijen terug naar de database
def writeBudgetChanges(inputdata):
    logging.debug("Begin functie: writeBudgetChanges")

    # Parameters
    endpoint = 'begrotingsstaten/UntypedDataSet'

    try:
        # Inladen en verwijderen oude regels
        r = executeApiCommand('R', endpoint=endpoint, data={'begrotingsjaar': config.settings['jaar']})
        df = pd.DataFrame(r['value'])
        if df.shape[0] > 1:
            executeApiCommand('D', endpoint=endpoint, data={'id': df['id'].tolist()})
            logging.info(f"Klaar met verwijderen {df.shape[0]} oude regels")

        # Filter bijgewerkte regels en schrijf die terug
        df = inputdata[inputdata['Begrotingsjaar'] == config.settings['jaar']]
        df.columns = df.columns.str.lower()
        executeApiCommand('C', endpoint=endpoint, data=df.to_dict('list'))

        logging.info(f"Klaar met wegschrijven {df.shape[0]} nieuwe regels")
        if logging.root.level == logging.DEBUG:
            df.to_csv(os.path.join(config.settings['outputdir'], 'debug', 'writeBudgetChanges.csv'))
    except Exception as e:
        logging.critical(f"Fout tijdens wegschrijven bijgewerkte regels begrotingsstaten: {str(e)}")
    logging.debug("Einde functie: writeBudgetChanges")

# Exporteer de open data-bestanden
def exportBudgetData(dataset):
    logging.debug("Begin functie: exportBudgetData")

    try:
        # Bestandsnaam opbouwen
        filename = os.path.join(config.settings['outputdir'], f"Begrotingsstaten 2013-{dataset['Begrotingsjaar'].astype(int).max()}")

        # Exporteer naar XLSX, ODS, CSV en JSON
        dataset.to_csv(filename+'.csv', index=False)
        dataset.to_json(filename+'.json', index=False, orient='table')
        #dataset.to_excel(filename+'.xlsx', index=False) # too slow...
        wb = Workbook()
        data = [dataset.columns.tolist(), ] + dataset.values.tolist()
        wb.new_sheet('Data', data=data)
        wb.save(filename+'.xlsx')
        #dataset.to_excel(filename+'.ods', index=False, engine='odf') # too slow...
        dataset = dataset.astype(str)
        d = OrderedDict()
        d.update({"Data": data})
        save_data(filename+'.ods', d)

        logging.info("Open data-bestanden (begrotingsstaten) voor de website succesvol geëxporteerd")
    except Exception as e:
        logging.critical(f"Fout tijdens exporteren begrotingsstaten: {str(e)}")
    logging.debug("Einde functie: exportBudgetData")
