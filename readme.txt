README bij de pijplijn

Versie oktober 2023

De pijplijn neemt twee soorten input (opties in config)
swr
datahub

Als allebei die bestanden zijn ingeladen (en opgeslagen in de map 'medium' - dwz medium tussen vies en schoon), is er de optie
combo
Om een gecombineerd bestand te maken dat de versies uit de swr en datahub aan elkaar knoopt en extra checks doet. 

De optie (in config) premiefilter = True levert twee sets output op: één met, en één zonder premies.
Om de premies weg te filteren is het nodig om bepaalde posten met N-kaders aan te passen. 
Specifiek zijn er artikelen en artikelonderdelen waaronder een subpost wordt verwijderd als je de premies meeneemt, en waarvoor een nieuwe totaaltelling nodig is. 
Op dit moment leveren we de juiste totaaltelling nog los aan, in een bestand in de map 'supplementen'.

Verder zijn er sanity checks op de totale dataset in sanitycheck.py, die nog niet in de pijplijn zijn geïntegreerd. 


Als alles goed is, krijg je hiermee de output: 
- Budgettaire tabel [lopend jaar']
- Bestand donut incl premies
- Bestand donut excl premies
- Budgettaire tabellen [lopend jaar] incl premies (in 4 bestandstypen)
- Budgettaire tabellen [lopend jaar] excl premies (in 4 bestandstypen)
- Eventueel Begrotingsstaten*



*Om dit bestand te genereren heb je IBOS-standen nodig. Die kan je ophalen in de GAO met SQL Server, en opslaan in map 'mock'. 



 