#!/usr/bin/env python3
# =============================================================================
__author__ = 'Erik Joost van Dongen en R.J. Peters'
__copyright__ = 'Copyright 2024, Ministerie van Financiën'
__license__ = 'CC-BY'
__version__ = '0.2.0'
__maintainer__ = 'Erik Joost van Dongen en R.J. Peters'
__email__ = 'e.j.dongen@minfin.nl, r.j.peters@minfin.nl'
__status__ = 'Prototype'
# =============================================================================
"""Browserrobot om gerelateerde informatie te uploaden"""
# =============================================================================

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time
import os
import logging

### === CONFIGURATIES === ###

# Hier het pad van de map met je authenticatiebestand invullen
authentication_folder = r'H:\Mijn Documenten\Data\Scripts\hulpscripts\auth'
# Het pad van de map met de te uploaden bestanden
inputfolder = r'O:\DGRB\Bz\BBH\Naar een digitale begroting\Rijksfinancien.nl\RVS advies\2023\2de sub'
# Ga je op productie of acceptatie uploaden?
versie = 'acceptatie' # productie of acceptatie
browser = 'Firefox' # of 'Edge' of 'Chrome'
label_website_versie = 'Nader Rapport' # Tekst die uiteindelijk op de website moet
filetype = 'pdf'


### === OVERIGE CONFIGURATIES === ###
categorie_dictionary = {"bn":"Beslisnota's",
                        "bnjv":"Beslisnota's Jaarverslag Rijk 2022",
                        "rvs":"Advies Raad van State",
                        "mbv":"Monitor Brede Welvaart",
                        "veeg":"Veegbrief"}

hoofdstuklijst = ['I',
                    'IIA',
                    'IIB',
                    'III',
                    'IV',
                    'V',
                    'VI',
                    'VII',
                    'VIII',
                    'IX',
                    'X',
                    'XII',
                    'XIII',
                    'XIV',
                    'XV',
                    'XVI',
                    'XVII',
                    'A',
                    'B',
                    'C',
                    'J',
                    'K',
                    'L']

# Fase/documenttype in hoofdletters, wet = w, memorie van toelichting = mvt
fase_dictionary = {
                    'O1mvt':'1supp',
                    'O1w':'1supp (wet)',
                    'O2mvt':'2supp',
                    'O2w':'2supp (wet)',
                    'BPmvt':'belastingplan (mvt)',
                    'BPsb':'belastingplan (sb)',
                    'BPw':'belastingplan (vvw)',
                    'FJV':'financieel jaarverslag',
                    'FJVb':'financieel jaarverslag (bijlage)',
                    'ISBw':'isb (wet)',
                    'ISBmvt':'isb (mvt)',
                    'JV':'jv',
                    'MJN':'miljoenennota',
                    'MJNb':'miljoenennota (bijlage)',
                    'NJN':'najaarsnota',
                    'OWBmvt':'owb',
                    'OWBw':'owb (wet)',
                    'PSmvt':'psupp',
                    'PSw':'psupp (wet)',
                    'SWw':'sw',
                    'SWmvt':'sw (mvt)',
                    'VJN':'voorjaarsnota'
                  }

# De url waar het uiteindelijk allemaal om te doen is
url_prod = 'https://www.rijksfinancien.nl/'
url_acc = 'www-acc.rijksfinancien.nl/'



def get_password(bestandsnaam):
    logging.debug("Begin van functie: get_password")
    with open(f'{authentication_folder}\\{bestandsnaam}', 'r') as f:
        lines = f.readlines()
        username = lines[0].strip()    # 'strip' om newline character \n te verwijderen
        password = lines[1].strip()
    
    logging.debug("Einde van functie: get_pasword")
    return username, password

def login(driver, username, password):
    logging.debug("Begin van functie: login")
    driver.get(f'{url}/user/login')
    username_form = driver.find_element(By.ID, 'edit-name')
    username_form.send_keys(username)
    password_form = driver.find_element(By.ID, 'edit-pass')
    password_form.send_keys(password)
    login_button = driver.find_element(By.ID, 'edit-submit')
    WebDriverWait(driver, 1000000).until(EC.element_to_be_clickable((By.ID, 'edit-submit'))).click()
    
    logging.debug("Einde van functie: login")
    return


def invullen(url, filename, echt = False):
    logging.debug("Begin van functie: invullen")
    
    try:
        ### Categorieën definieren (om te vertalen tussen de 'korte' versie in de bestandsnamen en de 'lange' versie in drupal)
        categorie_dictionary = {"bn":"Beslisnota's","bnjv":"Beslisnota's Jaarverslag Rijk 2022","rvs":"Advies Raad van State","mbv":"Monitor Brede Welvaart","veeg":"Veegbrief"}
        # Labels definiëren (hoe de link op de site eruit komt te zien)

        
        ### Alle variabelen uit de bestandsnaam halen ###
        dummy = filename.split('.')[0]
        filenamelist = dummy.split('_')

        labelnaam = dummy
        jaarnaam = filenamelist[2]
        categorienaam = categorie_dictionary[filenamelist[0]]
        fasenaam = fase_dictionary[filenamelist[1]]
        hoofdstuknaam = filenamelist[3]
        urinaam = ''   #Nu nog leeg, misschien later wel nodig


        ### Velden ophalen en invullen ###
        # Label 
        label = driver.find_element(By.ID, 'edit-label-0-value')
        label.clear()
        label.send_keys(labelnaam)

        # Jaar
        jaar = driver.find_element(By.ID, 'edit-jaar-0-value')
        jaar.clear()
        jaar.send_keys(jaarnaam)

        # Categorie
        categorie = driver.find_element(By.ID, 'edit-category')
        selectcategorie = Select(categorie)
        selectcategorie.select_by_visible_text(categorienaam)
        
        # Fase
        fase = driver.find_element(By.ID, 'edit-phase')
        selectfase = Select(fase)
        selectfase.select_by_visible_text(fasenaam)

        # Hoofdstuk
        hoofdstuk = driver.find_element(By.ID, 'edit-hoofdstuk-minfin-id-0-value')
        hoofdstuk.clear()
        hoofdstuk.send_keys(hoofdstuknaam)

        # URI (nog blanco, komt potentieel ooit van pas)
        uri = driver.find_element(By.ID, 'edit-uri-0-value')
        uri.clear()
        uri.send_keys(urinaam)


        ### Bestand uploaden ###

        # Volledige pad ophalen
        inputpath = os.path.join(inputfolder, filename)
        file_input = driver.find_element(By.CSS_SELECTOR, "input[type='file']")
        file_input.send_keys(inputpath)
        WebDriverWait(driver, 1000000).until(EC.presence_of_element_located((By.CSS_SELECTOR, "input[name='file_0_remove_button']")))
            # Hij wacht hier op het verschijnen van de 'verwijderen'-knop. Die verschijnt alleen als de upload is gelukt. 

        ### OP DE KNOP DRUKKEN ###
        if echt == False: 
            logging.debug("Einde van functie: invullen (niets gedaan, optie echt == False)")
            return 1
        
        

        opslaanknop = driver.find_element(By.ID, 'edit-submit')
        WebDriverWait(driver, 1000000).until(EC.element_to_be_clickable((By.ID, 'edit-submit'))).click()

        # De robot (en de website) even tijd geven
        time.sleep(2) #TODO: random interval!
        # Checken of je naar de juiste pagina bent gestuurd
        newurl = driver.current_url 
        if not f'{url}admin/kamerstuk_related_information/related_item' in newurl:
            logging.critical(f"Verkeerde URL na 'opslaan': {newurl}. Verwachtte:'{url}/admin/kamerstuk_related_information/related_item'")
            return

        WebDriverWait(driver, 1000000).until(EC.element_to_be_clickable((By.ID, 'edit-label')))
        labelveld = driver.find_element(By.ID, 'edit-label')
        labelveld.clear()
        labelveld.send_keys(labelnaam)

        WebDriverWait(driver, 1000000).until(EC.element_to_be_clickable((By.ID, 'edit-filter'))).click()

        
        # Naïeve versie: niet wachten tot edit-knop überhaupt verschijnt...
        link = driver.find_element(By.LINK_TEXT, "Edit")
        # ALS HIER EEN FOUTMELDING, DAN UIT DE FUNCTIE EN DE BESTANDSNAAM PRINTEN
        link.click()
        
        newurl = driver.current_url 
        if not f'{url}admin/kamerstuk_related_information/related_item' in newurl:
            logging.critical(f"Verkeerde URL na 'opslaan': {newurl}")
            return
        
        label = driver.find_element(By.ID, 'edit-label-0-value')
        label.clear()
        label.send_keys(label_website_versie)
        
        opslaanknop = driver.find_element(By.ID, 'edit-submit')
        WebDriverWait(driver, 1000000).until(EC.element_to_be_clickable((By.ID, 'edit-submit'))).click()
        WebDriverWait(driver, 1000000).until(EC.presence_of_element_located((By.ID, 'edit-filter')))
        
        
    except Exception as e:
        logging.error(f"Fout bij bestand {filename}: {str(e)}")

    logging.debug("Einde van functie: invullen")
    return 0


def valideer_bestand(filename) -> int:
    # Een aantal checks om te valideren of een bestandsnaam aan het gewenste format voldoet. 
    # Als alles ok is, is de output van deze functie 0
    # Als er iets fout is, krijg je een output van 1 of meer, en een foutmelding in de logger. 
    
    logging.debug("Begin functie: valideer_bestand")
    # Bestand heeft format: categorie_fase_jaar_hoofdstuk.extensie
    
    exitcode = 0
            
    dummy = filename.split('.')[0]
    filenamelist = dummy.split('_')

    # Twee checks op lengte van bestandsnaam
    if len(filenamelist) > 4:
        logging.warning(f"Bestandsnaam te lang. Verwacht format: categorie_fase_jaar_[hoofdstuk].extensie")
        return 1
    
    if len(filenamelist) < 3:
        logging.warning(f"Bestandsnaam te lang. Verwacht format: categorie_fase_jaar_[hoofdstuk].extensie")
        return 1
    
    # Check op categorie
    categorienaam = filenamelist[0]
    if not categorienaam in categorie_dictionary.keys():
        logging.warning(f"Waarschuwing: bestand {filename} heeft onjuiste categorie. Ongeldige input: {categorienaam}")
        exitcode += 1    
    
    # Check op fase
    fasenaam = filenamelist[1]
    if not fasenaam in fase_dictionary.keys():
        logging.warning(f"Waarschuwing: bestand {filename} heeft onjuiste fase. Ongeldige input: {fasenaam}")
        exitcode += 1
        
    # Check op jaartal
    jaarnaam = filenamelist[2]
    if not len(jaarnaam) == 4:
        logging.warning(f"Waarschuwing: bestand {filename} heeft onjuist jaartal. Verwacht vier karakters, vindt er {len(jaarnaam)}")
        exitcode += 1

    # Check op hoofdstuknaam
    hoofdstuknaam = filenamelist[3]
    if not hoofdstuknaam in hoofdstuklijst: 
        logging.warning(f"Waarschuwing: bestand {filename} heeft onjuiste hoofdstuknaam. Ongeldige input: {hoofdstuknaam}")
        exitcode += 1

    # Check op extensie
    if not filename.split('.')[-1] == filetype:
        logging.warning(f"Waarschuwing: bestand {filename} heeft verkeerde extensie. Verwacht {filetype}, is {filename.split('.')[-1]}")
        exitcode += 1        
    

    logging.debug("Einde functie: valideer_bestand")
    return exitcode



def main():
    

    # De juiste versie initialiseren
    if versie == 'productie':
        url = url_prod
    elif versie == 'acceptatie':
        url = url_acc
    else: 
        print('Geen bestaande versie')
    
    # Browser openen
    if browser == 'Firefox':
        driver = webdriver.Firefox()
    elif browser == 'Edge':
        driver = webdriver.Edge()
    
    # Authenticatie eerste lijn (voor acc)
    if url == url_acc:
        username1, password1 = get_password('authenticate1.txt')
        driver.get(f'https://{username1}:{password1}@{url}')
        url = f'https://{url}'

    # Wachtwoord ophalen en inloggen
    username2, password2 = get_password('authenticate2.txt')
    login(driver, username2, password2)
            
    # Bestandsnamen ophalen en valideren
    filelist = os.listdir(inputfolder)
    for file in filelist: 
        if valideer_bestand(filelist) == 0:
            pass
        else: 
            logging.warning("Onjuiste bestandsnaam: {file}")


    for file in filelist:
        invullen(url, file, echt = True)


    return


if __name__ == "__main__":
    main()
