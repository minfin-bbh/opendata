#!/usr/bin/env python3
# =============================================================================
__author__ = 'Erik Joost van Dongen'
__copyright__ = 'Copyright 2024, Ministerie van Financiën'
__license__ = 'CC-BY'
__version__ = '0.2.0'
__maintainer__ = 'Erik Joost van Dongen'
__email__ = 'e.j.dongen@minfin.nl'
__status__ = 'Prototype'
# =============================================================================
"""Script om RVS-adviezen te downloaden"""
# =============================================================================

import os
import requests


mydir = r"H:\Mijn Documenten\Data\test"
hoofdstukken = ['I', 'IIA', 'IIB', 'III', 'IV', 'V', 'VI', 'VII', 'VIII', 'IX', 'IXA', 
                'IXB', 'X', 'XII', 'XIII', 'XIV', 'XV', 'XVI', 'XVII', 'XVIII', 'XIX', 'A', 'B', 'C', 'F', 'H', 'J', 'K','L']
kamerstukken = {
    '2015': {
        'OWB': 34000,
        'O1' : 34210,
        'O2' : 34350,
        'JV' : 34475,
        'BP' : 34002,
    },
     '2016': {
        'OWB': 34300,
        'O1' : 34485,
        'O2' : 34620,
        'JV' : 34725,
        'BP' : 34302,
    },
 '2017': {
        'OWB': 34550,
        'O1' : 34730,
        'O2' : 34845,
        'JV' : 34950, 
        'BP' : 34552,
    },
 '2018': {
        'OWB': 34775,
        'O1' : 34960,
        'O2' : 35095,
        'JV' : 35200,
        'BP' : 34785,
    },
 '2019': {
        'OWB': 35000,
        'O1' : 35210,
        'O2' : 35350,
        'JV' : 35470,
        'BP' : 35026,
    },
 '2020': {
        'OWB': 35300,
        'O1' : 35450,
        'O2' : 35650,
        'JV' : 35830,
        'BP' : 35302,
    },
 '2021': {
        'OWB': 35570,
        'O1': 35850,
        'O2': 35975,
        'JV': 36100,
        'BP': 35572,
    },
 '2022': {
        'OWB': 35925,
        'O1': 36120,
        'O2': 36250,
        'JV': 36360,  
        'BP': 35927,
    },
'2023': {
        'OWB': 36200,
        'O1' : 36350,
        'BP': 36202,
        'O2': 36470,
        'JV': 36560
    },
'2024': {
        'OWB': 36410,
        'O1': 36550
    },
}
proxies = {
"http": "http://172.30.102.24:8080",
"https": "http://172.30.102.24:8080",
}




def downloader(jaar, fase):

    try:
        os.makedirs(os.path.join(mydir, str(jaar)), exist_ok=True)
        os.makedirs(os.path.join(mydir, str(jaar), fase), exist_ok=True)
    except Exception as e:
        print(f"Fout bij maken van nieuwe map: {e}")

    for hoofdstuk in hoofdstukken:
        try:
            link = f'https://zoek.officielebekendmakingen.nl/kst-{kamerstukken[jaar][fase]}-{hoofdstuk}-3.pdf'
            r = requests.get(link, headers={'User-Agent': 'Chrome'}, proxies = proxies, verify = False, timeout=(5, 5))
            if r.ok:
                filename = link.rsplit('/', 1)[1]
                filename = os.path.join(mydir, jaar, fase, filename)
                open(filename, 'wb').write(r.content)
            else:
                print(f"Fout bij downloaden van bestand {link}")

        except Exception as e:
            print(f"Fout bij downloaden van bestand {link}: {e}")
    return

def main():
    downloader('2024', 'O1')

if __name__ == '__main__':
    main()