#!/usr/bin/env python3
# =============================================================================
__author__ = 'Christian C. Schouten'
__copyright__ = 'Copyright 2022, Ministerie van Financiën'
__license__ = 'CC-BY'
__version__ = '0.2.0'
__maintainer__ = 'Rik Peters'
__email__ = 'r.j.peters@minfin.nl'
__status__ = 'Prototype'
# =============================================================================
"""Source file for the main script releasing open data"""
# =============================================================================
import opendata.config as config
from opendata.datainput import readInputFile, readCompleteDataSet
from opendata.datacleaning import cleanInputData, setLabelColumns, checkInternalConsistency, manipulateData, cleanDataBasic1, cleanDataBasic2, premieFilter
from opendata.dataoutput import writeDataChanges, exportWebsiteFiles
from opendata.budgetstatement import loadBudgetData, writeBudgetChanges, exportBudgetData
import pandas as pd
import logging
import os
import time
# =============================================================================

# Voeg indexnummer toe
def setIndex(df):
    logging.debug("Begin functie: setIndex")

    jaar = config.settings['jaar']
    try:
        # Indexwaarde is: Jaar.Hoofdstuknummer.Volgnummer
        for i in range(df.shape[0]):
            df.loc[i, 'Index'] = f'{jaar}.' + f"{int(df.loc[i, 'Hoofdstuknummer'].strip('ABC')):02d}" + f'{i+1:04d}'

        if logging.root.level == logging.DEBUG:
            df.to_csv(os.path.join(config.settings['outputdir'], 'debug', 'setIndex.csv'))
    except Exception as e:
        logging.critical(f"Fout tijdens indexeren van databestand: {str(e)}")
    logging.debug("Einde functie: setIndex")
    return df['Index']

# Loop door de boom om alle ouder-kind relaties in te stellen
def setParent(df):
    logging.debug("Begin functie: setParent")

    try:
        # Begin met het hoofdstuknummer als topniveau
        df['Parent'] = df['Index'].str[:-4]

        for i in range(df.shape[0]):
            dft = None
            # IBOS-nummer met alle zes niveaus
            if df.loc[i, 'Detailnummer'] != 0:
                # Zoek zelfde IBOS-nummer op instrumentniveau
                dft = df[((df['VUO'] == df.loc[i, 'VUO']) & (df['Hoofdstuknummer'] == df.loc[i, 'Hoofdstuknummer']) & (df['Artikelnummer'] == df.loc[i, 'Artikelnummer']) & (df['Artikelonderdeelnummer'] == df.loc[i, 'Artikelonderdeelnummer']) & (df['Instrumentnummer'] == df.loc[i, 'Instrumentnummer']) & (df['Detailnummer'] == 0))]
                # Zoek zelfde IBOS-nummer op artikelonderdeelniveau indien instrumentniveau mist
                if dft.shape[0] < 1:
                    dft = df[((df['VUO'] == df.loc[i, 'VUO']) & (df['Hoofdstuknummer'] == df.loc[i, 'Hoofdstuknummer']) & (df['Artikelnummer'] == df.loc[i, 'Artikelnummer']) & (df['Artikelonderdeelnummer'] == df.loc[i, 'Artikelonderdeelnummer']) & (df['Instrumentnummer'] == 0))]
                # Zoek zelfde IBOS-nummer op artikelniveau indien artikelonderdeelniveau mist
                if dft.shape[0] < 1:
                    dft = df[((df['VUO'] == df.loc[i, 'VUO']) & (df['Hoofdstuknummer'] == df.loc[i, 'Hoofdstuknummer']) & (df['Artikelnummer'] == df.loc[i, 'Artikelnummer']) & (df['Artikelonderdeelnummer'] == 0))]
            # IBOS-nummer met vijf niveaus
            elif df.loc[i, 'Instrumentnummer'] != 0:
                # Zoek zelfde IBOS-nummer op artikelonderdeelniveau
                dft = df[((df['VUO'] == df.loc[i, 'VUO']) & (df['Hoofdstuknummer'] == df.loc[i, 'Hoofdstuknummer']) & (df['Artikelnummer'] == df.loc[i, 'Artikelnummer']) & (df['Artikelonderdeelnummer'] == df.loc[i, 'Artikelonderdeelnummer']) & (df['Instrumentnummer'] == 0))]
                # Zoek zelfde IBOS-nummer op artikelniveau indien artikelonderdeelniveau mist
                if dft.shape[0] < 1:
                    dft = df[((df['VUO'] == df.loc[i, 'VUO']) & (df['Hoofdstuknummer'] == df.loc[i, 'Hoofdstuknummer']) & (df['Artikelnummer'] == df.loc[i, 'Artikelnummer']) & (df['Artikelonderdeelnummer'] == 0))]
            # IBOS-nummer met vier niveaus
            elif df.loc[i, 'Artikelonderdeelnummer'] != 0:
                # Zoek zelfde IBOS-nummer op artikelniveau
                dft = df[((df['VUO'] == df.loc[i, 'VUO']) & (df['Hoofdstuknummer'] == df.loc[i, 'Hoofdstuknummer']) & (df['Artikelnummer'] == df.loc[i, 'Artikelnummer']) & (df['Artikelonderdeelnummer'] == 0))]

            # Stel de ouder in, indien gevonden
            if dft is not None and dft.shape[0] > 0:
                df.loc[i, 'Parent'] = df.loc[dft.index.to_list()[0], 'Index']

        if logging.root.level == logging.DEBUG:
            df.to_csv(os.path.join(config.settings['outputdir'], 'debug', 'setParent.csv'))
    except Exception as e:
        logging.critical(f"Fout tijdens instellen ouder-kind relaties: {str(e)}")
    logging.debug("Einde functie: setParent")
    return df['Parent']

# Main functie: lees in en verwerk de verschillende fasebestanden en verzorg de output
def main():
    logging.debug("Begin functie: main")
    fase = config.settings['fase']
    dataset = None
    starttime = time.perf_counter()

    try:
        # Bepaal welke bronbestanden ingelezen moeten worden
        if 'input' in config.settings['program']:
            owbcols = ['OWBMin2', 'OWBMin1', 'StandOWB', 'OWBPlus1', 'OWBPlus2', 'OWBPlus3', 'OWBPlus4']
            o1cols = ['MutatieOWB', 'StandVB', 'MutatieO1', 'StandO1', 'MutatiePlus1', 'MutatiePlus2', 'MutatiePlus3', 'MutatiePlus4']
            pscols = ['StandO1enISB', 'MutatiePSupp', 'StandPSupp']
            o2cols = ['StandVO1', 'MutatieO2MJN', 'MutatieO2Overig', 'StandO2']
            jvcols = ['RealisatieMin4', 'RealisatieMin3', 'RealisatieMin2', 'RealisatieMin1', 'Realisatie', 'Verschil']

            # Ontwerpbegroting

            if fase >= config.PS:
                # Inlezen
                psdata = readInputFile(config.PS)
                psdata = cleanInputData(psdata, config.PS)
                
                dataset = psdata.copy()

                # Velden zetten
                dataset['Index'] = 'Nieuw in PS'

                
            if fase >= config.OWB:
                # Inlezen
                owbdata = readInputFile(config.OWB)
                owbdata = cleanInputData(owbdata, config.OWB)

                # Samenvoegen
                dataset = owbdata.copy()

                # Velden zetten
                dataset['Index'] = 'Nieuw in OWB'

                # Checks uitvoeren
                checkInternalConsistency(dataset, config.OWB)

            # Eerste supp
            if fase >= config.O1:
                # Inlezen
                o1data = readInputFile(config.O1)
                o1data = cleanInputData(o1data, config.O1)

                # Samenvoegen
                dataset = dataset.merge(o1data, 'outer', on=['Hoofdstuknummer', 'Artikelnummer', 'VUO', 'Artikelonderdeelnummer', 'Instrumentnummer', 'Detailnummer'], suffixes=('', '_o1'))

                # Velden zetten
                dataset['Index'] = dataset['Index'].fillna('Nieuw in O1')
                dataset['Artikelnaam'] = dataset['Artikelnaam'].fillna(dataset['Artikelnaam_o1'])
                dataset['IBOSnummer'] = dataset['IBOSnummer'].fillna(dataset['IBOSnummer_o1'])
                dataset['Totaal'] = dataset['Totaal'].fillna(dataset['Totaal_o1'])
                dataset['Omschrijving'] = dataset['Omschrijving'].fillna(dataset['Omschrijving_o1'])
                dataset['Begrotingsjaar'] = dataset['Begrotingsjaar'].fillna(dataset['Begrotingsjaar_o1'])

                # Checks uitvoeren
                checkInternalConsistency(dataset, config.O1)

            
            # Tweede supp
            if fase >= config.O2:
                # Inlezen
                o2data = readInputFile(config.O2)
                o2data = cleanInputData(o2data, config.O2)

                # Samenvoegen
                dataset = dataset.merge(o2data, 'outer', on=['Hoofdstuknummer', 'Artikelnummer', 'VUO', 'Artikelonderdeelnummer', 'Instrumentnummer', 'Detailnummer'], suffixes=('', '_o2'))

                # Velden zetten
                dataset['Index'] = dataset['Index'].fillna('Nieuw in O2')
                dataset['Artikelnaam'] = dataset['Artikelnaam'].fillna(dataset['Artikelnaam_o2'])
                dataset['IBOSnummer'] = dataset['IBOSnummer'].fillna(dataset['IBOSnummer_o2'])
                dataset['Totaal'] = dataset['Totaal'].fillna(dataset['Totaal_o2'])
                dataset['Omschrijving'] = dataset['Omschrijving'].fillna(dataset['Omschrijving_o2'])
                dataset['Begrotingsjaar'] = dataset['Begrotingsjaar'].fillna(dataset['Begrotingsjaar_o2'])

                # Checks uitvoeren
                checkInternalConsistency(dataset, config.O2)

            # Jaarverslag
            if fase >= config.JV:
                # Inlezen
                jvdata = readInputFile(config.JV)
                jvdata = cleanInputData(jvdata, config.JV)

                # Samenvoegen
                dataset = dataset.merge(jvdata, 'outer', on=['Hoofdstuknummer', 'Artikelnummer', 'VUO', 'Artikelonderdeelnummer', 'Instrumentnummer', 'Detailnummer'], suffixes=('', '_jv'))

                # Velden zetten
                dataset['Index'] = dataset['Index'].fillna('Nieuw in JV')
                dataset['Artikelnaam'] = dataset['Artikelnaam'].fillna(dataset['Artikelnaam_jv'])
                dataset['IBOSnummer'] = dataset['IBOSnummer'].fillna(dataset['IBOSnummer_jv'])
                dataset['Totaal'] = dataset['Totaal'].fillna(dataset['Totaal_jv'])
                dataset['Omschrijving'] = dataset['Omschrijving'].fillna(dataset['Omschrijving_jv'])
                dataset['Begrotingsjaar'] = dataset['Begrotingsjaar'].fillna(dataset['Begrotingsjaar_jv'])

                # Checks uitvoeren
                checkInternalConsistency(dataset, config.JV)

            # Sorteer dataset
            dataset = dataset.sort_values(by = ['Hoofdstuknummer', 'Artikelnummer', 'VUO', 'Artikelonderdeelnummer', 'Instrumentnummer', 'Detailnummer'], ascending=[True, True, False, True, True, True])
            dataset = dataset.reset_index(drop=True)

            # Stel de indexkolom in en de ouder-kindrelatie
            dataset['Index'] = setIndex(dataset)
            dataset['Parent'] = setParent(dataset)

            # Vul de namen aan vanuit de ouder-kindrelatie
            if config.settings['inputstyle'] == 'swr':
                dataset = setLabelColumns(dataset)

            # Output opschonen
            dataset = cleanDataBasic1(dataset, config.Totaal) # Extra schoonmaakronde over totaal
            dataset = cleanDataBasic2(dataset, config.Totaal) # Extra schoonmaakronde over totaal
            if config.settings['inputstyle']=='dh':
                dataset = manipulateData(dataset) # Bewerkingen op de data ivm presentatie
            dataset['Hoofdstuknummer'] = dataset.apply(lambda x: config._hoofdstukToLabel.get(x['Hoofdstuknummer']), axis=1) # Label op hoofdstuk ipv numeriek
            dataset['Hoofdstuknaam'] = dataset.apply(lambda x: config._hoofdstukToName.get(x['Hoofdstuknummer']), axis=1) # Uitgeschreven naam
            columns = ['Index', 'Parent', 'IBOSnummer', 'VUO', 'Totaal', 'Begrotingsjaar', 'Hoofdstuknummer', 'Hoofdstuknaam', 'Artikelnummer', 'Artikelnaam', 'Artikelonderdeelnummer', 'Artikelonderdeelnaam', 'Instrumentnummer', 'Instrumentnaam', 'Detailnummer', 'Detailnaam']
            fasecols = []
            if fase >= config.O1: fasecols.extend(o1cols)
            if fase >= config.PS: fasecols.extend(pscols)
            if fase >= config.O2: fasecols.extend(o2cols)
            if fase >= config.JV: fasecols.extend(jvcols)
            columns.extend(fasecols) # Juiste aantal kolommen voor fase
            dataset = dataset[columns] # Drop alle tijdelijke kolommen
            dataset = dataset[dataset.columns[~dataset.columns.str.endswith(('_ibos', '_o1', '_o2', '_jv'))]]
            fasecols.extend(['Begrotingsjaar', 'Artikelnummer', 'Artikelonderdeelnummer', 'Instrumentnummer', 'Detailnummer']) # intcols
            for col in fasecols:
                dataset[col] = dataset[col].fillna(0).astype(float) # Voorkom nan bij later toegevoegde regels
                dataset[col] = dataset[col].round(0).astype(str).str.split('.', expand=True)[0] # Verwijder pandas '.0'

            # Exporteer jaarbestand
            dataset.to_csv(os.path.join(config.settings['outputdir'], f"Budgettaire Tabel {config.settings['jaar']}.csv"), index=False)

        # Voeg de nieuwe data in bij de oude
        if 'merge' in config.settings['program']:
            try:
                logging.info(f"Totale dataset van {config.settings['jaar']} t/m fase {config._faseToName.get(config.settings['fase'])} bevat {dataset.shape[0]} regels.")
                olddata = readCompleteDataSet() # Alles ophalen uit de API
                olddata = olddata[olddata['Begrotingsjaar'] != config.settings['jaar']] # Verwijder huidig jaar om dubbele rijen te voorkomen
                dataset = pd.concat([dataset, olddata], ignore_index=True)
            except Exception as e:
                logging.warning(f"Fout tijdens merge: {str(e)}")

        # Schrijf de veranderingen terug naar de database en exporteer bestanden
        if 'output' in config.settings['program']:
            try:
                writeDataChanges(dataset)
            except Exception as e:
                logging.warning(f"Fout tijdens output (communicatie met API): {str(e)}")
            exportWebsiteFiles(dataset)

        # Genereer de begrotingsstaten
        if 'budgetstatement' in config.settings['program']:
            budget = loadBudgetData()
            try:
                writeBudgetChanges(budget)
            except Exception as e:
                logging.warning(f"Fout tijdens ophalen begrotingsstaten (communicatie met API): {str(e)}")
            exportBudgetData(budget)

        if logging.root.level == logging.DEBUG:
            dataset.to_csv(os.path.join(config.settings['outputdir'], 'debug', 'main.csv'))
    except Exception as e:
        logging.critical(f"Fout tijdens main: {str(e)}")
    logging.debug("Einde functie: main")
    endtime = time.perf_counter()
    print(f"Time: {endtime - starttime:0.4f} seconds")
    return 0


if __name__ == "__main__":
    main()
