#!/usr/bin/env python3
# =============================================================================
__author__ = 'Christian C. Schouten'
__copyright__ = 'Copyright 2022, Ministerie van Financiën'
__license__ = 'CC-BY'
__version__ = '0.2.0'
__maintainer__ = 'Rik Peters'
__email__ = 'r.j.peters@minfin.nl'
__status__ = 'Prototype'
# =============================================================================
"""Source file for the main script releasing open data"""
# =============================================================================
import opendata.config as config
from opendata.datainput import readInputFile, readCompleteDataSet
from opendata.datacleaning import cleanInputData, setLabelColumns, checkInternalConsistency, manipulateData, cleanDataBasic1, cleanDataBasic2
from opendata.dataoutput import writeDataChanges, exportWebsiteFiles
from opendata.budgetstatement import loadBudgetData, writeBudgetChanges, exportBudgetData
from opendata.consolidator import consolidateDatahub, cleanDatahub
from opendata.merger import normalizeIbos, merge
from opendata.setindex import setIndex, setParent
from opendata.getdata import getdata
import pandas as pd
import logging
import os
import time
# =============================================================================



# Main functie: lees in en verwerk de verschillende fasebestanden en verzorg de output
def main():

    ### === Initialiseren: ophalen gegevens uit config bestand === ###

    logging.debug("Begin functie: main")
    fasenaam = config._faseToName[config.settings['fase']]
    fase = config.settings['fase']
    starttime = time.perf_counter()
    jaar = config.settings['jaar']
    outputdir = config.settings['outputdir']
    inputdir = config.settings['inputdir']

    ### === Bestanden uit datahub lezen en opschonen === ###
    if 'datahub' in config.settings['inputtype']:
        
        try:
            dhdata = consolidateDatahub(fase=fasenaam, jaar=jaar)
            dhdata = cleanDatahub(dhdata)
            logging.info("Klaar met het inlezen en opschonen van datahub-bestanden")
        except Exception as e: 
            logging.CRITICAL(f"Fout tijdens inlezen, combineren en/of opschonen van datahub-bestanden: {str(e)}")
        if 'save' in config.settings['program']:
            dhdata.to_csv(os.path.join(inputdir,'medium',f"{jaar}_{fasenaam}_dh.csv"), index = False)

    ### === Bestand uit swr lezen en opschonen === ###
    if 'swr' in config.settings['inputtype']:
        try:
            swrdata = readInputFile(inputstyle='swr')
            swrdata = cleanInputData(swrdata, fase = fasenaam, inputstyle='swr')
            
            # Sorteer dataset
            swrdata = swrdata.sort_values(by = ['Hoofdstuknummer', 'Artikelnummer', 'VUO', 'Artikelonderdeelnummer', 'Instrumentnummer', 'Detailnummer'], ascending=[True, True, False, True, True, True])
            swrdata = swrdata.reset_index(drop=True)

            # Stel de indexkolom in en de ouder-kindrelatie
            swrdata['Index'] = setIndex(swrdata)
            swrdata['Parent'] = setParent(swrdata)

            # Vul de namen aan vanuit de ouder-kindrelatie
            swrdata = setLabelColumns(swrdata)

            if 'save' in config.settings['program']:
                swrdata.to_csv(os.path.join(inputdir,'medium',f"{jaar}_{fasenaam}_swr.csv"), index = False)

        except Exception as e:
            logging.CRITICAL(f"Fout tijdens inlezen en/of opschonen van swr-bestand: {str(e)}")


    ### === Combo van SWR en datahub maken === ###
    if 'combo' in config.settings['inputtype']:
        try:
            if not 'save' in config.settings['program']:
                dataset = merge(dhdata, swrdata, fase = fasenaam)
            else:
                dataset = merge(fase = fasenaam)

            checkInternalConsistency(dataset, fase=fase)
            
        except Exception as e:
            logging.CRITICAL(f"Fout tijdens inlezen van gecombineerd swr/datahub-bestand")



        

        try:

            # Sorteer dataset
            dataset = dataset.sort_values(by = ['Hoofdstuknummer', 'Artikelnummer', 'VUO', 'Artikelonderdeelnummer', 'Instrumentnummer', 'Detailnummer'], ascending=[True, True, False, True, True, True])
            dataset = dataset.reset_index(drop=True)

            # Stel de indexkolom in en de ouder-kindrelatie
            dataset['Index'] = setIndex(dataset)
            dataset['Parent'] = setParent(dataset)

            # Output opschonen
            dataset = cleanDataBasic2(dataset, config.Totaal) # Extra schoonmaakronde over totaal
            
            
        ### === TWEE OPTIES: CUMULATIEF OF NIET CUMULATIEF === ###

            if config.settings['cumulatief']==False:
                dataset = getdata(fase=fase, cumulatief=False, inputdata = dataset)
            elif config.settings['cumulatief']==True:
                dataset = getdata(fase=fase, cumulatief=True, inputdata = dataset)


            ### === INCLUSIEF & EXCLUSIEF PREMIES === ###

            df_incl = manipulateData(dataset, inclusief = True)
            df_excl = manipulateData(dataset, inclusief = False)

            df_incl['Index'] = setIndex(df_incl)
            df_incl['Parent'] = setParent(df_incl)

            df_excl['Index'] = setIndex(df_excl)
            df_excl['Parent'] = setParent(df_excl)

            df_incl['Hoofdstuknummer'] = df_incl.apply(lambda x: config._hoofdstukToLabel.get(x['Hoofdstuknummer']), axis=1) # Label op hoofdstuk ipv numeriek
            df_excl['Hoofdstuknummer'] = df_excl.apply(lambda x: config._hoofdstukToLabel.get(x['Hoofdstuknummer']), axis=1) # Label op hoofdstuk ipv numeriek

            df_incl['Hoofdstuknaam'] = df_incl.apply(lambda x: config._hoofdstukToName.get(x['Hoofdstuknummer']), axis=1) # Uitgeschreven naam
            df_excl['Hoofdstuknaam'] = df_excl.apply(lambda x: config._hoofdstukToName.get(x['Hoofdstuknummer']), axis=1) # Uitgeschreven naam



            if config.settings['premiefilter']==False:
                df_excl.to_csv(os.path.join(config.settings['outputdir'], f"Budgettaire Tabel {config.settings['jaar']}.csv"), index=False)

            elif config.settings['premiefilter']==True:
                df_excl.to_csv(os.path.join(config.settings['outputdir'], f"Budgettaire Tabel {config.settings['jaar']}_excl.csv"), index=False)
                df_incl.to_csv(os.path.join(config.settings['outputdir'], f"Budgettaire Tabel {config.settings['jaar']}_incl.csv"), index=False)



      



            # Voeg de nieuwe data in bij de oude
            if 'merge' in config.settings['program']:
                try:
                    logging.info(f"Totale dataset van {config.settings['jaar']} t/m fase {config._faseToName.get(config.settings['fase'])} bevat {dataset.shape[0]} regels.")
                    olddata = readCompleteDataSet() # Alles ophalen uit de API
                    olddata = olddata[olddata['Begrotingsjaar'] != config.settings['jaar']] # Verwijder huidig jaar om dubbele rijen te voorkomen
                    dataset = pd.concat([dataset, olddata], ignore_index=True)
                except Exception as e:
                    logging.warning(f"Fout tijdens merge: {str(e)}")

            # Schrijf de veranderingen terug naar de database en exporteer bestanden
            if 'output' in config.settings['program']:
                try:
                    writeDataChanges(dataset)
                except Exception as e:
                    logging.warning(f"Fout tijdens output (communicatie met API): {str(e)}")

                exportWebsiteFiles(dataset)

            # Genereer de begrotingsstaten
            if 'budgetstatement' in config.settings['program']:
                budget = loadBudgetData()
                try:
                    writeBudgetChanges(budget)
                except Exception as e:
                    logging.warning(f"Fout tijdens ophalen begrotingsstaten (communicatie met API): {str(e)}")
                exportBudgetData(budget)

            if logging.root.level == logging.DEBUG:
                dataset.to_csv(os.path.join(config.settings['outputdir'], 'debug', 'main.csv'))
        except Exception as e:
            logging.critical(f"Fout tijdens main: {str(e)}")
    logging.debug("Einde functie: main")
    endtime = time.perf_counter()
    print(f"Time: {endtime - starttime:0.4f} seconds")
    return 0



if __name__ == "__main__":
    main()
